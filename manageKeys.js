
import React, { Component } from "react";
import AdminLayout from "../../components/adminLayout"
import { connectToDatabase, getUser } from '../../utils/db'
import { getSession } from '../../lib/iron'

export default function ManageKeys ({ isConnected }) {
  const handleSubmit = async function () {
    const publicKey = document.getElementById("publicKey").value;
    
    await fetch('/api/admin/addpublickey?publicKey='+publicKey, {method: 'POST'});
    
    document.getElementById("publicKey").value = "";
  }
  
  return(
    <AdminLayout>
      <div className="manageKeys">
        <h2>Add a public key</h2><br/>
        <textarea id="publicKey" name="publicKey" style={{width:"400px", height:"200px"}}/><br/>
        <button type="button" style={{width:"400px"}} onClick={()=>{handleSubmit()}}>Submit</button>
      </div>
    </AdminLayout>
  )
}

export async function getServerSideProps(context) {
  const { client } = await connectToDatabase()
  
  const isConnected = await client.isConnected() // Returns true or false
  
  let tasks = []
  const session = await getSession(context.req)
  
  let user = null
  if (session)
    user = await getUser(session.username)
    
  let admin = false
  if (user)
    admin = user.role=="admin"
    
  //TODO: if not admin

  return {
    props: { isConnected },
  }
}

