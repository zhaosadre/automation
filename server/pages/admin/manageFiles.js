import Router from 'next/router'
import React, { Component } from "react";
import AdminLayout from "../../components/adminLayout"
import { connectToDatabase, getUser, getAllFolders} from '../../utils/db'
import { getSession } from '../../lib/iron'
import 'react-confirm-alert/src/react-confirm-alert.css'
import { openPanel, closePanel, getSeletedFolders, closeAllPanels, toggleAllFolders, getSelectedFileName, closePanelOnly, removeCheckItems} from '../../utils/help'
import { charpAlert, charpAlertDupleButtons} from '../../utils/charpAlert'


export default function ManageFiles ({ isConnected, folders, admin}) {

    const path = require('path');
    let filesOfFolder = []
    let balancedFolders = []
    let selectedFolder=""

    if (!admin){
        return <div className="authenticationDiv">"User not authenticated"</div>
    }
    
    const deleteSelectedFolders = async function(document){
        let deleteOK=false
        let message=""

        closePanelOnly(["uploadDivId", "cleanUnrecordFolderId"], document)
        if(!folders && !folders.length){
            return charpAlert("Delete folder", "There is not folder in the server.",
            () => {
                deleteOK=false
                message="There is not workflow"
            })
        }

        const selectedFolders = getSeletedFolders(folders, document)
        if (selectedFolders && selectedFolders.length){
            return charpAlertDupleButtons("Delete folder", "Do you really want to delete the folder?",
            async ()=>{
                for (let i=0; i<selectedFolders.length; i++){
                    let data = await fetch('/api/folder/'+selectedFolders[i], {method: 'DELETE'})
                    if(data.status!=200){
                        deleteOK=false
                        message=(await data.json()).message
                      } else {deleteOK=true}
                }
                if(deleteOK){
                    charpAlert("Delete folder", "Folder deleted successfully",
                    ()=>{Router.push("/admin/manageFiles"), removeCheckItems(document)})
                } else {
                    charpAlert("Delete folder", "Folder deleted failed: "+message)
                }
            },()=>{})
        } else {
            deleteOK=false
            message="Please select the folder."
        }
        if(deleteOK){
            charpAlert("Delete folder", "Folder deleted successfully",
            ()=>{Router.push("/admin/manageFiles"), removeCheckItems(document)})
        } else {
            charpAlert("Delete folder", "Folder deleted failed: "+message,
            ()=>{
                // Router.push("/admin/manageFiles"), removeCheckItems(document)
            })
        }
        return
    } 

    const uploadFolders = async function(document){
        closeAllPanels(document)
        openPanel(["uploadDivId"], document)
        if(document.getElementById("uplodadFilesInputId")){
            document.getElementById("uplodadFilesInputId").value=""
        }
    }

    const deletingDcmFiles = async function(document){
        let deleteOK = false
        let message=""

        let data = await fetch('/api/folder/'+selectedFolder, {method: 'GET'})
        if (data.status == 200){
            const dcmFiles = (await data.json()).files  
            if (dcmFiles && dcmFiles.length){
                let checkedFiles = getSelectedFileName(dcmFiles, document, "filenameCheckbox", "")

                if ( checkedFiles && checkedFiles.length){
                    return charpAlertDupleButtons( "Delete DCM file","Do you really want to delete the DCM file?",
                        async () => {
                            for(let i=0; i<checkedFiles.length; i++){
                                let data = await fetch('/api/folder/'+selectedFolder+'/'+checkedFiles[i]["fileName"], {method: 'DELETE'})
                                if(data.status!=200){
                                    deleteOK=false
                                    noDeletedFile.push(checkedFiles[i]["fileName"])
                                    message = (await data.json()).message
                                } else { deleteOK=true }
                            }
                            if (deleteOK){
                                charpAlert("Delete DCM file", "File deleted successfully", ()=>{closePanel(["dcmDataInFolderDivId"], document)})
                            } else {
                                charpAlert("Delete DCM file", "File delete failed:"+message, ()=>{closePanel(["dcmDataInFolderDivId"], document)})
                            }
                        },
                        ()=>{
                            closePanel(["dcmDataInFolderDivId"], document)
                        })
                }
            } else {
                deleteOK=false
                message = "No DCM file found for this folder."
            }
        } else {
            deleteOK=false
            message = (await data.json()).message
        }

        if (deleteOK){
            charpAlert("Delete DCM file", "File deleted successfully", ()=>{closePanel(["dcmDataInFolderDivId"], document)})
        } else {
            charpAlert("Delete DCM file", "File delete failed:"+message, ()=>{closePanel(["dcmDataInFolderDivId"], document)})
        }
        return
    }

    const OpenFolderDcmFiles = async function(folderName, document){
        
        selectedFolder=folderName
        const data = await fetch('/api/folder/'+folderName, {method:'GET'})
        if(data.status==200){
            let files = (await data.json()).files
            if (files && files.length){
                createDcmFilesTable(files, document)
            } else {
                return charpAlert("Open folder", "The selected folder is empty.")
            }
        } else {
            let message = await data.json()
            if (message.message){
                message = message.message
            }
            return charpAlert("DCM files", message,
                ()=>{
                    cleanCheckedFileItems(document)
                    closePanel(["dcmDataInFolderDivId"], document)})
        }
    }

    const uploadingFiles = async function(dirname, document) {
        const formData=new FormData(document.getElementById("uploadFormId"))
        const result = await fetch("/api/folder/"+dirname, {method:'POST', body: formData})
        if(result.status==200){
            closePanel(["uploadDivId"], document)
            Router.push("/admin/manageFiles")
            return
        }
        else {
            const message = (await result.json()).message
            return charpAlert("Uploading files", 
            'Uploading files failed: '+message,
            ()=>{Router.push("/admin/manageFiles")}
            )
        }
    }

    const uploadFiles = async function (document) {
        
        const dirname=document.getElementById("uploadFolderNameId").value.trim()
        if(!dirname || dirname===0 || /[^A-Za-z0-9_]/.test(dirname)) {
            return  charpAlert("Upload files", "The folder name must only contain the characters A-Z, 0-9 and _")
        }

        const data = await fetch('/api/folders', {method: 'GET'})
        if (data.status==200){
            const getfolders=(await data.json()).folders
            if (getfolders && getfolders.length){
                if (getfolders.includes(dirname)){
                    charpAlertDupleButtons("Upload files", 
                    "There is a folder with the same name already on the server. Do you want to upload your files into that folder? If files with the same name already exist, they will be overwritten.", 
                    async ()=>{
                        uploadingFiles(dirname, document)
                    },()=>{})
                } else {
                    uploadingFiles(dirname, document)
                }
            } else {
                uploadingFiles(dirname, document)
            }
            return
        } else {
            return charpAlert("Upload files", 
            'Upload failed: '+(await data.json()).message,
            ()=>{}
            )
        }
        return false
    }

    const cleanCheckedFileItems = function(document){
        if (filesOfFolder && filesOfFolder.length){
            for(let i=0; i<filesOfFolder.length; i++){
                if(document.getElementById("filenameCheckbox"+filesOfFolder[i].fileName)){
                    (document.getElementById("filenameCheckbox")+filesOfFolder[i].fileName).checked=false
                }
            }
        }
    }

    const createDcmFilesTable = async function(files, document){
        const tbodyRef = document.getElementById('dcmFilesTbodyId')
        while (tbodyRef.firstChild) {
        tbodyRef.removeChild(tbodyRef.firstChild)
        }

        if(files && files.length) {
            files.forEach(el => {
                const row = document.createElement("tr")
                const tdFileInput = document.createElement("td")
                const cellCheckboxFileName = document.createElement('input')
                cellCheckboxFileName.type='checkbox'
                cellCheckboxFileName.className="cbFiles"
                cellCheckboxFileName.id="filenameCheckbox" + el["fileName"]
                tdFileInput.appendChild(cellCheckboxFileName)
                row.appendChild(tdFileInput)

                const tdModality = document.createElement("td")
                const cellModality = document.createTextNode(el["modality"])
                tdModality.appendChild(cellModality)
                row.appendChild(tdModality)
    
                const tdName = document.createElement("td")
                const cellName = document.createTextNode(el["fileName"])
                tdName.appendChild(cellName)
                row.appendChild(tdName)

                tbodyRef.appendChild(row)
            })
        }
       openPanel(["dcmDataInFolderDivId"], document)
    }

    const cleanUnrecordFolder = async function(document){
        if (balancedFolders && balancedFolders.length){
            for (let i=0; i<balancedFolders.length; i++){
                const deleteName=null
                if (balancedFolders[i]["db"] == "-" && balancedFolders[i]["server"] != "-"){
                    deleteName = balancedFolders[i]["server"]
                } else if (balancedFolders[i]["db"] != "-" && balancedFolders[i]["server"] == "-"){
                    deleteName = balancedFolders[i]["db"]
                } else {
                    deleteName=""
                }
                if (deleteName){
                    const data=await fetch('/api/folder/'+deleteName, {method: "DELETE"})
                    if (data.status==200){
                        charpAlert("Clean unrecorded folder", "Folders cleaned successfully.", 
                        async ()=>{Router.push("/admin/manageFiles")})
                    } else {
                        const message = (await data.json()).message
                        charpAlert("Clean unrecorded folder", "Folders cleaned failt: "+message)
                    }
                }
            }
        }
        closePanel(["cleanUnrecordFolderId"], document)
        return
    }

    const getFolderStatus = async function(){
        let checkedFolders = []
        const data = await fetch('/api/folders', {method: 'GET'})
        let sfolders=(await data.json()).folders
        if (sfolders || sfolders.length>0) {
            let sFoldersSort = sfolders.sort()
            let dbFoldersSort = folders.map((e)=>e.folderName).sort()

            let i =0
            let j = 0
            let item = {}
            while ((i<sFoldersSort.length) || (j<dbFoldersSort.length)) {
                if(j==dbFoldersSort.length || sFoldersSort[i]<dbFoldersSort[j]){
                    item = {"db": "-", "server": sFoldersSort[i]}
                    i++
                } else if(i==sFoldersSort.length || sFoldersSort[i]>dbFoldersSort[j]){
                    item = {"db": dbFoldersSort[j], "server":"-"}
                    j++
                } else {
                    item = {"db": dbFoldersSort[j], "server":sFoldersSort[i]}
                    j++ 
                    i++
                }
                checkedFolders.push(item)
            }    
        }
        return checkedFolders
    } 

    const openCleanFoldersTable = async function (document){
        closePanel(["uploadDivId", "dcmDataInFolderDivId"], document)
        balancedFolders = await getFolderStatus()
        const tbodyRef = document.getElementById('cleanUnrecordTbodyId')
        while (tbodyRef.firstChild) {
            tbodyRef.removeChild(tbodyRef.firstChild)
        }
        if(balancedFolders && balancedFolders.length) {
            balancedFolders.forEach(el => {
                const row = document.createElement("tr")

                const tdDB = document.createElement("td")
                const cellDB = document.createTextNode(el["db"])
                tdDB.appendChild(cellDB)
                row.appendChild(tdDB)
    
                const tdServer = document.createElement("td")
                const cellServer = document.createTextNode(el["server"])
                tdServer.appendChild(cellServer)
                row.appendChild(tdServer)

                tbodyRef.appendChild(row)
            })
        }
        openPanel(["cleanUnrecordFolderId"], document)
        return 
    }

    let folderRows = (folders && folders.length) ?
        folders.map(el => <tr key={"folderrow"+el.folderName} onClick={()=>OpenFolderDcmFiles(el.folderName, document)}>
            <td>
                <input type="checkbox" id={"foldercheckbox" + el.folderName} onClick={(e)=>e.stopPropagation()}/>
            </td>
            <td>{el.folderName}</td>
            <td>{el.creator}</td>
            <td>{el.createDate}</td>
        </tr>
    ) : null
    return (
        <AdminLayout>
            <div className="filesLayout">
                <div className="filesList">
                    <table>
                        <tbody>
                            <tr>
                                <td key="uploadFiles">
                                    <button onClick={()=>{uploadFolders(document)}}>Upload files</button>
                                </td>
                                <td key="deleteFolders">
                                    <button onClick={()=>{deleteSelectedFolders(document)}}>Delete folder</button>
                                </td>
                                <td key="synconizeFolder">
                                  <button onClick={()=>{openCleanFoldersTable(document)}}>Clean unrecorded folder</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <h2 className="magfolderHeaderText">All patient folders</h2>
                    <table className="magfolderTable"> 
                        <thead>
                            <tr>
                                <th><input type="checkbox" id="selectAll" onClick={()=>{toggleAllFolders(document, folders)}}/></th>
                                <th>Folder name</th>
                                <th>Creator</th>
                                <th>Create date</th>
                            </tr>
                        </thead>
                        <tbody className="magPatientsTableBody" id="magPatientsTableBodyId">
                            {folderRows}
                        </tbody>
                    </table>
                </div>
            </div>
            <div className="uploadDiv" id="uploadDivId" hidden>
                <h2 className="uploadFileHeaderText">Upload DCM files</h2>
                <p>Please specify the folder name on the server:
                <input type="text" id="uploadFolderNameId" className="folderName"/>
                </p>
                <form encType="multipart/form-data" method="post" id="uploadFormId" hidden action="">
                    <p>Select the files on your computer to upload:
                    <input id="uplodadFilesInputId" type="file" name="uploadedFiles" multiple/>
                    </p>
                    <div className="bottom-buttons">
                    <input type='button' onClick={()=>{uploadFiles(document)}} value="Upload"/>
                    <button type="reset" onClick={()=>closePanel(["uploadDivId"], document)}>Cancel</button>
                    </div>
                </form>
            </div>
            <div className="dcmDataInFolderDiv" id="dcmDataInFolderDivId" hidden>
                <div className="dcm-files">
                <h2 className="dcmDataInFolderText" id="dcmDataInFolderTextId">Please select the files to delete</h2>
                <div className="managerDcmFile">
                    <table className="folderDcmFilesTable" id="folderDcmFilesTableId">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Modality</th>
                            <th>File name</th>
                        </tr>
                    </thead>
                    <tbody className="dcmFilesTbody" id="dcmFilesTbodyId"></tbody>
                    </table>
                </div>
                </div>
                <div className="bottom-buttons">
                    <button onClick={()=>closeAllPanels(document)}>Cancel</button>
                    <button onClick={()=>deletingDcmFiles(document)}>Delete</button>  
                </div>
            </div>
            <div className="cleanUnrecordFolder" id="cleanUnrecordFolderId" hidden>
                <div className="dcm-files">
                <h2 className="cleanUnrecordFolderText" id="cleanUnrecordFolderTextId">Clean unrecorded folders</h2>
                <label className="cleanRecordfolderMessage" id="cleanRecordfolderMessageId">Clean the unsafe data if there are "-" in the table (important!)</label>
                <div className="cleanUnrecordTableDiv">
                    <table className="cleanUnrecordTable" id="cleanUnrecordTableId">
                    <thead>
                        <tr>
                            <th>Folders in DB</th>
                            <th>Folder in server</th>
                        </tr>
                    </thead>
                    <tbody className="cleanUnrecordTbody" id="cleanUnrecordTbodyId"></tbody>
                    </table>
                </div>
                </div>
                <div className="bottom-buttons">
                    <button onClick={()=>cleanUnrecordFolder(document)}>Clean</button>  
                </div>
            </div>
        </AdminLayout>
    )
}

/**
 * 
 * @param {*} context 
 */
export async function getServerSideProps(context) {
    const { client } = await connectToDatabase()
  
    const isConnected = await client.isConnected() // Returns true or false
    const session = await getSession(context.req)

    let user = null
    if (session) {
      user = await getUser(session.username)
    }

    let admin = false
    if (user)
      admin = user.role=="admin"

    let folders = []
    // let username = user.username
    if (session){
        folders = await getAllFolders()
    }
    if (folders && folders.length){
        for(let i=0; i<folders.length; i++){
            folders[i] = {
                folderName: folders[i].folderName.toString(),
                creator: folders[i].creator.toString(),
                createDate: folders[i].createDate.toString()
            }
        }
    }
    return {
      props: { isConnected, folders, admin},
    }
}