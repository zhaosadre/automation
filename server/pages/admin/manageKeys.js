
import React, { Component } from "react";
import AdminLayout from "../../components/adminLayout"
import { connectToDatabase, getUser } from '../../utils/db'
import { getSession } from '../../lib/iron'
import { charpAlert } from "../../utils/charpAlert";

export default function ManageKeys ({ isConnected, admin}) {

  if (!admin){
    return <div className="authenticationDiv">"User not authenticated"</div>
  }

  const handleSubmit = async function () {
    const publicKey = document.getElementById("publicKey").value;
    
    let data = await fetch('/api/admin/addpublickey?publicKey='+publicKey, {method: 'POST'});
    if(data.status!=200){
      const message = (await data.json()).message
      return charpAlert("Add public key", "Public key added failt: "+message)
    }
    document.getElementById("publicKeyId").value = "";
  }
  
  return(
    <AdminLayout>
      <div className="manageKeys">
        <h2 className="manageKeyText">Add a public key</h2><br/>
        <textarea id="publicKeyId" className="publicKey"/><br/>
        <div className="manageKeyButton">
        <button type="button" onClick={()=>{handleSubmit()}}>Submit</button>
        </div>
      </div>
    </AdminLayout>
  )
}

export async function getServerSideProps(context) {
  const { client } = await connectToDatabase()
  
  const isConnected = await client.isConnected() // Returns true or false
  
  let tasks = []
  const session = await getSession(context.req)
  
  let user = null
  if (session)
    user = await getUser(session.username)
    
  let admin = false
  if (user)
    admin = user.role=="admin"
    
  //TODO: if not admin

  return {
    props: { isConnected, admin},
  }
}

