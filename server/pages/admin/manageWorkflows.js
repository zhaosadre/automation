import Router from 'next/router'
import React, { Component } from "react";
import AdminLayout from "../../components/adminLayout"
import { connectToDatabase, getAllWorkflows, getUser, getUsers } from '../../utils/db'
import { showUserSelectionDialog, getSelectedUsers, getNumberOfSelectedWorkflows, toggleAll, openPanel, closePanel, closeThenOpenPanel, getSeletedPatient, closeAllPanels, getSelectedContours, getEntryText, workflowToData, getSelectedFileName, workflowToLinks, isStringValid, removeCheckItems, getSeletedWorkflows } from '../../utils/help'
import { getSession } from '../../lib/iron'
import * as echarts from "echarts";
import 'react-confirm-alert/src/react-confirm-alert.css'
import { charpAlert, charpAlertDupleButtons} from '../../utils/charpAlert'


export default function ManageWorkflows ({ isConnected, tasks, users, admin}) {
  
  let workflowIdShownInGraph = null
  let selectedWorkflowId = null
  let patients = null
  let patientContours = null
  let selectedPatient = null
  let selectedContours = null
  let patientDcmfiles = null

  if (!admin){
    return <div className="authenticationDiv">"User not authenticated"</div>
  }

  const openWorkflowGraphClick = async function(worfklowId, document) {

    if (!worfklowId){
      return charpAlert("Open workflow graph", "No selected workflow")
    }

    closeAllPanels(document)
    workflowIdShownInGraph = worfklowId
    let workflow = await fetch('/api/workflow/'+worfklowId, {method: 'GET'});
    workflow = await workflow.json();

    const data = workflowToData(workflow);
    const links = workflowToLinks(workflow, data);

    const canDeleteWorkFlowEntry = function(entryId) {
      const ws = workflow.filter((w)=>w._id==entryId)
      // this should not happen...
      if(ws.length==0) {
        return false
      }
      // entry must have a user assigned to it
      if(!ws[0].users || ws[0].users.length==0) {
        return false
      }
      // entry must not be parent of other entries
      const children = workflow.filter((w)=> w.parents.includes(entryId))
      if(children.length>0) {
        return false
      }
      // everything ok, can delete
      return true
    }

    const option = {
        title: {
            text: workflow[0].info
        },
        tooltip: {},
        animationDurationUpdate: 1500,
        animationEasingUpdate: 'quinticInOut',
        series: [
            {
                type: 'graph',
                layout: 'none',
                symbolSize: 50,
                roam: true,
                label: {
                    show: true
                },
                edgeSymbol: ['circle', 'arrow'],
                edgeSymbolSize: [4, 10],
                edgeLabel: {
                    fontSize: 20
                },
                data,
                links,
                lineStyle: {
                    opacity: 0.9,
                    width: 2,
                    curveness: 0
                }
            }
        ]
    };
    
    window.addEventListener('contextmenu', function(event) {
      event.preventDefault();
    }, true); 

    // For workflow graph
    const panel = document.getElementById("workflowGraphId")
    panel.removeAttribute("hidden")
    const graphPanel = document.getElementById("workflowGraphPanelId")
    // Remove the echart from the panel to reset zoom, scroll etc.
    while(graphPanel.hasChildNodes()) {
      graphPanel.removeChild(graphPanel.childNodes[0])
    }
    graphPanel.removeAttribute("_echarts_instance_")

    const myChart = echarts.init(graphPanel)
    myChart.setOption(option);
    myChart.on('click', function (params) {
      selectedWorkflowId = params.data.id
      if (selectedWorkflowId){
        panel.setAttribute("hidden","")
        Router.push({
          pathname: '/viewer',
          query: { workflowEntryId: params.data.id }
        });
      }
    });

    // For assignment users to the workflow
    myChart.on('contextmenu', function(params){
      selectedWorkflowId = params.data.id
     
      console.log("Is workflow entry id " + selectedWorkflowId)

      if (selectedWorkflowId) {
          const contextMenu = document.getElementById("graphContextMenuId")
          contextMenu.style.left = params.event.event.offsetX+"px"
          contextMenu.style.top = params.event.event.offsetY+"px"
          contextMenu.removeAttribute("hidden")

          const deleteReviewerMenu = document.getElementById("deleteReviewerId")
          const workflowEntryCanBeDeleted = canDeleteWorkFlowEntry(selectedWorkflowId)
          if(workflowEntryCanBeDeleted) {
            deleteReviewerMenu.removeAttribute("hidden")
          }
          else {
            deleteReviewerMenu.setAttribute("hidden","")
          }

          const clickEventListener = (e) => {
            contextMenu.setAttribute("hidden","")
            document.removeEventListener("click", clickEventListener)
          }
          document.addEventListener("click", clickEventListener)
       }
      }
    )
  }

  const doDeleteReviewer = async function(document) {   
    await fetch('/api/workflowentry/'+selectedWorkflowId, {method: 'DELETE'});
    await openWorkflowGraphClick(workflowIdShownInGraph, document)
  }

  const deleteReviewer = function(document) {
    if (selectedWorkflowId) {
      return charpAlertDupleButtons("Delete reviewer", 
        "Do you really want to delete the reviewer from the workflow?",
        ()=>{doDeleteReviewer(document)},()=>{}
      )
    }
  }

  const cleanUnsafe = async function (document) {
    closeAllPanels(document)
    return charpAlertDupleButtons(
      'Clean unsafe',
      'Are you sure that you want to delete permanently unused files? This cannot be undone. This operation is unsafe. It may delete data not yet linked to a workflow.',
      ()=>{clean()},
      ()=>{}
    )
  }

  const clean = async function() {    
    if (getNumberOfSelectedWorkflows(tasks, document)>0) {
      let count = await fetch('/api/admin/cleanunsafe', {method: 'PUT'});
      if(count.status==200) {
        count = await count.json();  
        return charpAlert(
          'Clean unsafe',
          count + " data were permanently deleted.", {})
      } else {
        const message = (await count.json()).message
        return charpAlert("Clean unsafe", "Clean failed: "+message)
      }
    } else {
      return charpAlert(
        'Clean unsafe',
        'You have to select the workflow', {})
    }
  }

  const setWorkflowReviewer = async function(document) {
    const selectedUsers = getSelectedUsers(users, document)
  
    if (selectedUsers && selectedUsers.length>0) {  
      for (let i=0; i<selectedUsers.length; i++){
        await fetch('/api/workflowentry/'+selectedWorkflowId+"/reviewer?user="+selectedUsers[i], {method: 'POST'});
      }
      document.getElementById("assignUsersWfId").setAttribute("hidden","")
      await openWorkflowGraphClick(workflowIdShownInGraph, document)
    }
    else {
      return charpAlert("Set workflow experts", 'You have to select at least one user')}
  }

  const deleteWorkflow = async function(document) {
    let deleteOK = false
    let message = ""
    
    if (!tasks || !tasks.length ){
      return charpAlert("Delete workflow", "There is not workflow", 
      ()=>{
        deleteOK=false
        message="There is not workflow"
      })
    }

    const selectedWorkflows = getSeletedWorkflows(tasks, document)
    if (selectedWorkflows && selectedWorkflows.length) {
      return charpAlertDupleButtons("Delete workflow", "Do you really want to delete the workflow?",
        async ()=>{
          for(let i=0; i<selectedWorkflows.length;i++){
            let data = await fetch('/api/workflow/'+selectedWorkflows[i].workflowId, { method: 'DELETE' })
            if(data.status!=200){
              deleteOK=false
              message= (await data.json()).message
            } else {deleteOK=true}
          }
          if(deleteOK){
            charpAlert("Delete workflow", "Workflow deleted successfully", 
            ()=>{Router.push("/admin/manageWorkflows"), removeCheckItems(document)})
          } else {
            charpAlert("Delete workflow", "Workflow deleted failed: "+message, 
            ()=>{})
          }
      },()=>{})
    } else {
      deleteOK=false
      message = "Please select the workflow."
    }
    if(deleteOK){
      charpAlert("Delete workflow", "Workflow deleted successfully", 
      ()=>{Router.push("/admin/manageWorkflows"), removeCheckItems(document)})
    } else {
      charpAlert("Delete workflow", message, 
      ()=>{
        // Router.push("/admin/manageWorkflows"), removeCheckItems(document)
      })
    }
    return
  }
  
  const createDcmFilesTable = async function(document){
    const patientfolder = selectedPatient["foldername"]
    
    const tbodyRef = document.getElementById('dcmFilesTbodyId')
    while (tbodyRef.firstChild) {
      tbodyRef.removeChild(tbodyRef.firstChild)
    }
    const data = await fetch('/api/folder/'+patientfolder, {method:'GET'})
    if (data.status==401) {
      const message = (await data.json()).message
      return charpAlert("Create workflow", "Workflow created failed: "+ message, 
      ()=>{closePanel(["dcmDataInFolderDivId"], document)})
    }  
    patientDcmfiles = (await data.json()).files
    if(patientDcmfiles) {
      patientDcmfiles.forEach(el => {
        const row = document.createElement("tr")

        const tdInput = document.createElement("td")
        const cellCheckboxFiles = document.createElement('input')
        cellCheckboxFiles.type = 'checkbox'
        cellCheckboxFiles.className = "cbDcmFiles"
        cellCheckboxFiles.id = "dcmfilecheckbox"+el["fileName"]
        tdInput.appendChild(cellCheckboxFiles)
        row.appendChild(tdInput)

        const tdModality = document.createElement("td")
        const cellModality = document.createTextNode(el["modality"])
        tdModality.appendChild(cellModality)
        row.appendChild(tdModality)

        const tdName = document.createElement("td")
        const cellName = document.createTextNode(el["fileName"])
        tdName.appendChild(cellName)
        row.appendChild(tdName)
        
        tbodyRef.appendChild(row)
      })
    }
  }
  
  const createWorkflowByContours = async function(selectedPatient, document) {
    let folderName = ""
    if(selectedPatient){
      folderName = selectedPatient["foldername"]
    } else {
      return charpAlert("Create workflow", "Please click <Create workflow> to select a patient again.")
    }

    const wfDescription = getEntryText("wfDescriptionId", document)
    const wfLabel = getEntryText("wfLabelId", document)
    const rsFileName = await getSelectedFileName(patientDcmfiles, document, "dcmfilecheckbox", "RTSTRUCT")
    const ctFileName = await getSelectedFileName(patientDcmfiles, document, "dcmfilecheckbox", "CT")
    const patientId = selectedPatient["id"]
    const studyDate = selectedPatient["studydate"]
    const studyTime = selectedPatient["studytime"]
    const contours = JSON.stringify(selectedContours)

    if(!isStringValid(wfLabel)){
      return charpAlert("Create workflow", "The workflow label must only contain the characters A-Z, 0-9 and _")
    }
   
    if (ctFileName && rsFileName && ctFileName.length==1 && rsFileName.length==1) {
      const rsName = rsFileName[0]["fileName"]
      const ctName = ctFileName[0]["fileName"]

      if(!isStringValid(rsName)){
        return charpAlert("Create workflow", "RT STRUCT file name must only contain the characters A-Z, 0-9 and _")
      }
      if(!isStringValid(ctName)){
        return charpAlert("Create workflow", "CT file name must only contain the characters A-Z, 0-9 and _")
      }

      document.getElementById("dcmDataInFolderDivId").setAttribute("hidden", "")
      openPanel(["createProgressDivId"], document)
      const data = await fetch('/api/workflow/patient/'+ selectedPatient["id"] +"/new?folderName="+ folderName+"&ctFileName="+ctName+"&rsFileName="+rsName+"&patientId="+patientId+"&contours="+contours+"&studyDate="+studyDate+"&studyTime="+studyTime+"&wfDescription="+wfDescription+"&wfLabel="+wfLabel, {method: 'POST'})

      if (data.status!=200) {
        const message = (await data.json()).message
        return charpAlert("Create workflow", "Workflow created failed: "+message, 
          ()=>{
            closePanel(["createProgressDivId"], document)
          })
      } else {
        return charpAlert("Create workflow", "Workflow created successfully", 
        ()=> {
          // closePanel(["createProgressDivId"], document)
          closeAllPanels(document)
          Router.push("/admin/manageWorkflows")
        })
      }
    } else {
      charpAlert("Create workflow", 
      "Workflow creation failed: Please select only one RT STRUCT file and one CT file.",
      ()=>{removeCheckItems(document)})
    }
    return
  }

  const startCreateWorflow = async function(document) {
    let data = await fetch('/api/patients', {method: 'GET'})
    if (data.status==401) {
      const message = (await data.json()).message
      return charpAlert("Create workflow", "No patients found: "+message)
    }
    patients = await data.json()

    const tbodyRef = document.getElementById('patientTableBodyId')
    while (tbodyRef.firstChild) {
      tbodyRef.removeChild(tbodyRef.firstChild)
    }

    if(patients && patients.length) {
        patients.forEach(el => {
          const row = document.createElement("tr")

          const tdInput = document.createElement("td")
          const cellCheckBoxPatient = document.createElement('input')
          cellCheckBoxPatient.type = 'radio'
          cellCheckBoxPatient.name = "rdpatient"
          cellCheckBoxPatient.id = "patientradio" + el["name"]
          tdInput.appendChild(cellCheckBoxPatient)
          row.appendChild(tdInput)

          const tdName = document.createElement("td")
          const cellName = document.createTextNode(el["name"])
          tdName.appendChild(cellName)
          row.appendChild(tdName)

          const tdStudyDate = document.createElement("td")
          const cellStudyDate = document.createTextNode(el["studydate"])
          tdStudyDate.appendChild(cellStudyDate)
          row.appendChild(tdStudyDate)

          const tdId = document.createElement("td")
          const cellId = document.createTextNode(el["id"])
          tdId.appendChild(cellId)
          row.appendChild(tdId)

          const tdFolderName = document.createElement("td")
          const cellFolderName = document.createTextNode(el["foldername"])
          tdFolderName.appendChild(cellFolderName)
          row.appendChild(tdFolderName)

          tbodyRef.appendChild(row)
        }
      )
    }
    openPanel(["patientsDivId"], document)
    return
  }

  const openPatientContours = async function(document) { 
    if (patients.length==0){
      return charpAlert("Create workflow", "There is not patients data.")
    }

    selectedPatient = getSeletedPatient(patients, document)
    if (!selectedPatient){
      return charpAlert("Create workflow", "Please select one patient")
    } else {
      const studyDate = selectedPatient["studydate"]
      const studyTime = selectedPatient["studytime"]
      const folderName = selectedPatient["foldername"]

      let data = await fetch('/api/patient/'+selectedPatient["id"]+'/contours?studyDate='+studyDate+'&studyTime='+studyTime+'&folderName='+folderName, {method: 'GET'});      
      if (data.status==401) {
        const message = (await data.json()).message
        return charpAlert("Create workflow", "No contours for the patient: "+message, 
        ()=>{closePanel(["patientContourspanelId"], document)}
        )
      } else {
        patientContours = await data.json()
        const tbodyRef = document.getElementById('patientContoursTbodyId')
        while (tbodyRef.firstChild) {
          tbodyRef.removeChild(tbodyRef.firstChild)
        }

        if(patientContours && patientContours.length) {
          patientContours.forEach(el => {
            const row = document.createElement("tr")
  
            const tdInput = document.createElement("td")
            const cellCheckBoxContours = document.createElement('input')
            cellCheckBoxContours.type = 'checkbox'
            cellCheckBoxContours.className = "cbContours"
            cellCheckBoxContours.id = "contourcheckbox" + el["roiname"]
            tdInput.appendChild(cellCheckBoxContours)
            row.appendChild(tdInput)
  
            const tdName = document.createElement("td")
            const cellName = document.createTextNode(el["roiname"])
            tdName.appendChild(cellName)
            row.appendChild(tdName)

            const tdNumber = document.createElement("td")
            const cellNumber = document.createTextNode(el["roinumber"])
            tdNumber.appendChild(cellNumber)
            row.appendChild(tdNumber)
  
            tbodyRef.appendChild(row)
          })
        }
        return closeThenOpenPanel("patientsDivId", "patientContourspanelId", document)
      }
    }
  }

  const openInputFileName = async function(document) {
    selectedContours = getSelectedContours(patientContours, document)
    createDcmFilesTable(document)

    if (!selectedContours || !selectedContours.length){
      return charpAlert("Create workflow", "Please select the contours")
    } else {
      return openPanel(["dcmDataInFolderDivId"], document)
    }
  }

  // Table rows of the workflows
  let taskrows=(tasks && tasks.length) ?
    tasks.map(el => <tr key={"workflowrow"+el.workflowId} onClick={()=>openWorkflowGraphClick(el.workflowId, document)}>
          <td>
            <input type="checkbox" id={"workflowcheckbox"+el.workflowId} onClick={(e)=>e.stopPropagation()}/>
          </td>
          <td>{el.info}</td>
          <td>{el.patientName}</td>
          <td>{el.BodyPartExamined}</td>
          <td>{el.PatientSex}</td>
          <td>{el.PatientAge}</td>
          <td>{el.date}</td>
          <td>{el.workflowId}</td>
          <style jsx>{`
          td {
            cursor: pointer;
          }
        `}</style>
        </tr>
    ) : null

  //User table rows
  let userRows=(users && users.length) ?
    users.map(el => <tr key={"userrow"+el.username}>
      <td>
        <input type="checkbox" id={"usercheckbox" + el.username}/>
      </td>
      <td>{el.username}</td>
      <td>{el.role}</td>
    </tr>
  ) : null
  
  return (
    <AdminLayout>
      <div className="worflowLayout">
        <div className="workflowList">
          <table>
            <tbody>
            <tr>
              <td key="createWorkflow">
                <button onClick={()=>{startCreateWorflow(document)}}>Create workflow</button>
              </td>
              <td key="dwf">
                <button onClick={()=>{deleteWorkflow(document)}}>Delete workflow</button>
              </td>
              <td key="cunsafe">
                <button onClick={()=>{cleanUnsafe(document)}}>Clean unused data (unsafe)</button>
              </td>
            </tr>
            </tbody>
          </table>
          <h2 className="workflowsheadertext">All workflows</h2>
          <table className="workflowTable">
            <thead>
              <tr>
                <th><input type="checkbox" id="selectAll" onClick={()=>{toggleAll(document, tasks)}}/></th>
                <th>Description</th>
                <th>Patient</th>
                <th>Body part examined</th>
                <th>Sex</th>
                <th>Age</th>
                <th>Date</th>
                <th>Workflow id</th>
            </tr>
            </thead>
            <tbody>
                {taskrows}
            </tbody>
          </table>
        </div>
      </div>
      <div className="workflowGraph" id="workflowGraphId" hidden>
        <button onClick={()=>closePanel(["workflowGraphId", "assignUsersWfId"], document)}>Close</button>        
        <div className="workflowGraphPanel" id="workflowGraphPanelId"/>
      </div>
      <div className="contextMenu" id="graphContextMenuId" hidden>
        <ul>
          <li className="context-menu-item" id="addReviewerId" onClick={()=>showUserSelectionDialog(users, document)}>Add reviewer</li>
          <li className="context-menu-item-delete" id="deleteReviewerId"onClick={()=>deleteReviewer(document)}>Delete reviewer</li>
        </ul>
      </div>
      <div className="assignUsersWf" id="assignUsersWfId" hidden>
        <h2 className="headertext">Assignment: select users</h2>
        <div className="assignUsersList" id="assignUsersListId">
          <table className="usersTable">
            <thead>
              <tr>
                <th></th>
                <th>User name</th>
                <th>User role</th>
              </tr>
            </thead>
            <tbody>
              {userRows}
            </tbody>
          </table>
          <div className="buttons">
            <button onClick={()=>setWorkflowReviewer(document)}>Create</button>   
            <button onClick={()=>closePanel(["assignUsersWfId"], document)}>Cancel</button>
          </div>
        </div>   
      </div>
      <div className="patientContourspanel" id="patientContourspanelId" hidden>
        <h2 className="patientContourheadertext">Patient Contours</h2>
        <div className="ContoursContainerDiv">
          <div className="contoursTableDiv">
            <div className="contoursTable-cell">
              <table className="contoursModalTable">
                <thead>
                  <tr>
                    <th></th>
                    <th>ROI Name</th>
                    <th>ROI Number</th>
                  </tr>
                </thead>
                <tbody className="patientContoursTbody" id="patientContoursTbodyId">
                </tbody>
              </table>
            </div>
          </div>
          <div className="bottom-buttons">
            <div className="cell">
              <button onClick={()=>closePanel(["patientContourspanelId"], document)}>Cancel</button>
              <button onClick={()=>openInputFileName(document)}>Continue</button>
            </div>
          </div>
        </div>
      </div>
      <div className="patientsDiv" id="patientsDivId" hidden>
        <h2 className="patientsDivheadertext">Patients</h2>
        <div className="patientContainerDiv">
            <div className="patientTableDiv">
              <div className="patienttable-cell">
                <table className="patientsTable" id="patientsTableId">
                  <thead>
                    <tr>
                      <th></th>
                      <th>Name</th>
                      <th>Study Date</th>
                      <th>Id</th>
                      <th>Folder</th>
                    </tr>
                  </thead>
                  <tbody className="patientTableBody" id="patientTableBodyId">
                  </tbody>
                </table>         
              </div>
            </div>
            <div className="bottom-buttons">
              <button onClick={()=>closePanel(["patientsDivId"], document)}>Cancel</button>
              <button onClick={()=>openPatientContours(document)}>Continue</button>
            </div>
          </div>
        </div>
        <div className="dcmDataInFolderDiv" id="dcmDataInFolderDivId" hidden>
          <h2 className="workflowInfoText">Please enter the worflow description and label:</h2>
          <div className="dcmFilesDiv" id="dcmFilesDivId">
            <div className="wf-info">
              <div className="wfinfo-cell">
                <label className="wflabel">Workflow label:</label>
                <input type="text" id="wfLabelId" className="wfLabel" />
                <label className="wfdes">Workflow description: </label>
                <input type="text" id="wfDescriptionId" className="wfDescription" />
              </div>
            </div>
            <div className="dcm-files">
              <div>
              <h2 className="dcmDataInFolderText" id="dcmDataInFolderTextId">Select patient's RS and Ct file</h2>
              </div>
              <div className="fileName-cell">
                <table className="dcmFilesTable" id="dcmFilesTableId">
                  <thead>
                    <tr>
                      <th></th>
                      <th>File name</th>
                      <th>Modality</th>
                    </tr>
                  </thead>
                  <tbody className="dcmFilesTbody" id="dcmFilesTbodyId">
                  </tbody>
                </table>
              </div>
            </div>
            <div className="bottom-buttons">
              <div className="create-cell">
                <button onClick={()=>closeAllPanels(document)}>Cancel</button>
                <button onClick={()=>createWorkflowByContours(selectedPatient,document)}>Create</button>  
              </div>
            </div>
          </div>
        </div>
        <div className="createProgressDiv" id="createProgressDivId" hidden>
          Creating ........
        </div>
    </AdminLayout>
  )
}

/**
 * 
 * @param {*} context 
 */
export async function getServerSideProps(context) {
  const { client } = await connectToDatabase()
  
  const isConnected = await client.isConnected() // Returns true or false
  
  let tasks = []
  const session = await getSession(context.req)
  
  let user = null
  if (session)
    user = await getUser(session.username)
  
  let users = []
  users = (await getUsers()).map( (u)=>{ return { username: u.username, role: u.role} } )

  let admin = false
  if (user)
    admin = user.role=="admin"
  
  //TODO: if not admin

  if (session)
    tasks = await getAllWorkflows()
  if (tasks && tasks.length)
    for (let i=0; i<tasks.length; i++)
      tasks[i] = {
        workflowId:  tasks[i].workflowId.toString(),
        info: tasks[i].info, 
        BodyPartExamined: tasks[i].BodyPartExamined.toString(),
        patientName: tasks[i].PatientName.toString(),
        PatientSex: tasks[i].PatientSex.toString(),
        PatientAge: tasks[i].PatientAge.toString(),
        date: tasks[i].date.toString(),
        users: tasks[i].users==null ? "" : tasks[i].users.toString()
      }
  return {
    props: { isConnected, tasks, users, admin},
  }
}

