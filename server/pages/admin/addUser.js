
import React, { Component } from "react";
import { useUser } from '../../lib/hooks'
import Form from '../../components/form'
import AdminLayout from "../../components/adminLayout"
import { useState } from 'react'
import { useRouter } from 'next/router';
import { charpAlert } from '../../utils/charpAlert'

export default function AddUser() {
    const user = useUser({ redirectTo: '/login' })
    const [errorMsg, setErrorMsg] = useState('')
    const router = useRouter();

    async function handleSubmit(e) {
      e.preventDefault()

      if (errorMsg) setErrorMsg('')
      const body = {
        username: e.currentTarget.username.value,
        password: e.currentTarget.password.value,
        role: e.currentTarget.role.value,
      }

      if (body.password !== e.currentTarget.rpassword.value) {
        charpAlert("User password", "The passwords don't match", 
        ()=>{setErrorMsg(`The passwords don't match`)})
        return
      }

      try {
        const res = await fetch('/api/admin/signup', {
          method: 'POST',
          headers: { 'Content-Type': 'application/json' },
          body: JSON.stringify(body),
        })
        if (res.status === 200) {
          charpAlert("Signup", "Signup successfully", 
          ()=>{router.push('/')})
        } else {
          const message = (await res.json()).message
          charpAlert("Signup", "Signup failt: "+message)
        }
      } catch (error) {
        charpAlert("Signup", "An unexpected error happened occurred: "+error, 
        ()=>{setErrorMsg(error.message)})
      }
    }
  
    return (
      <AdminLayout>
        <div className="addUser">
          <h1>User management</h1>
          {!user ? null : (
            <div className="addUserDiv">
              <h2>Add new user</h2>
              <Form isLogin={false} errorMessage={errorMsg} onSubmit={handleSubmit} />
            </div>
          )}
        </div>
      </AdminLayout>
    )
}

