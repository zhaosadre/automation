import './styles/styles.css'

// This default export is required in a new `pages/_app.js` file.
export default function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />
}

// === Wei added ===
// MyApp.getInitialProps = async appContext => {
//   let pageProps = {}
//   if (appContext.Component.getInitialProps) {
//     pageProps = await appContext.Component.getInitialProps(appContext.ctx);
//   }
//   return {pageProps}
// } 