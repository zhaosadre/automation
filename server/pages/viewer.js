
import React from 'react'
import ReactDOM from "react-dom";
import { useUser } from '../lib/hooks'
import ViewerLayout from '../components/viewerLayout'
import dynamic from "next/dynamic";
import uniqBy from 'lodash/uniqBy';
import CharpDataLoader from '../lib/CharpDataLoader'
import { getSession } from '../lib/iron'

async function importLib() {
  const module = await import("@deffets/viewer")
  const HomePage = module.Viewer.HomePage
  const DataSet = module.Viewer.DataSet
  
  return {HomePage, DataSet};
}
 
export default class DCMViewer extends React.Component {
  constructor(props) {
    super(props)
    
    this.dataSet = null;
  }
  
  
  async componentDidMount () {
    let images = []
    let segs = []
    
    const Viewer = await importLib()

    const HomePage = Viewer.HomePage
    
    this.dataSet = Viewer.DataSet.getInstance()
    const dataLoader= CharpDataLoader.getInstance()

    dataLoader.setURL("/api")
    this.dataSet.setDataLoader(dataLoader)
    
    this.dataSet.loadStudyAsync(this.props.workflowEntryId)
    
    try {
      ReactDOM.render(
        <HomePage StudyInstanceUID={this.props.workflowEntryId}/>,
        document.getElementById('viewerID')
      );
    }
    catch (e) {
    
    }
  }
  
  async componentWillUnmount() {
    if(this.dataSet) {
      await this.dataSet.delete();
      this.dataSet = null;
    }
  }
  
  render() {
    const style = { width: '100%', height: '100%', position: 'relative' };
    
    return (
      <ViewerLayout>
        <p>Loading. Please wait...</p>
        <style>{`
          p {
            color: #FFFFFF;
          }
        `}</style>
      </ViewerLayout>
    )
  }
}

export async function getServerSideProps(context) {
  return {
    props: { workflowEntryId:context.query.workflowEntryId },
  }
}

