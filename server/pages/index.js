import { useUser } from '../lib/hooks'
import Layout from '../components/layout'
import { getSession } from '../lib/iron'
import { connectToDatabase, getWorkflowEntries, getUser } from '../utils/db'
import { useRouter } from 'next/router';

export default function Home({ isConnected, tasks, admin }) {
  const user = useUser()
  const router = useRouter();

  async function handleWorkflowClick(workflowEntryId) {
    router.push({
        pathname: '/viewer',
        query: { workflowEntryId }
      });
  }

  if (tasks && tasks.length)
    tasks = tasks.map(el => <tr key={"workflow"+el._id} onClick={()=>{handleWorkflowClick(el._id)}}>
          <td>{el.info}</td>
          <td>{el.BodyPartExamined}</td>
          <td>{el.PatientSex}</td>
          <td>{el.PatientAge}</td>
          <td>{el.date}</td>
          <style jsx>{`
          td {
            cursor: pointer;
          }
        `}</style>
      </tr>)
  
  return (
    <Layout admin={admin}>
      <div className="homeLayout">
        <div className="home">
          <h1>CHARP</h1>
          {isConnected ? null 
          : 
            (
              <h2 className="subtitle">
                Server error: Database connection failed.
              </h2>
            )
          }
          
          {!user ? null : (
            <div >
              <h2 className="activeheadertext">Workflows assigned to you</h2>
              <table className="workflowTable">
                <thead>
                  <tr><th>Description</th><th>Body part examined</th><th>Sex</th><th>Age</th><th>Date</th></tr>
                </thead>
                <tbody>
                {tasks}
                </tbody>
              </table>
            </div>
          )}
        </div>
      </div>
    </Layout>
  )
}

/**
 * 
 * @param {*} context 
 */
export async function getServerSideProps(context) {
  const { client } = await connectToDatabase()
  
  const isConnected = await client.isConnected() // Returns true or false
  
  let tasks = []
  const session = await getSession(context.req)
  if (session)
    tasks = await getWorkflowEntries(session.username)
    
  if (tasks && tasks.length)
    for (let i=0; i<tasks.length; i++)
      tasks[i] = {
        _id:  tasks[i]._id.toString(),
        info: tasks[i].info, 
        BodyPartExamined: tasks[i].BodyPartExamined, 
        PatientSex: tasks[i].PatientSex,
        PatientAge: tasks[i].PatientAge,
        date: tasks[i].date.toString(),
      }
  
  let user = null
  if (session)
    user = await getUser(session.username)
    
  let admin = false
  if (user)
    admin = user.role=="admin"
  
  return {
    props: { isConnected, tasks, admin },
  }
}

