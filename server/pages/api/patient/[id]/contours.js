import { checkSession } from '../../../../utils/session'
import { checkAccess } from '../../../../utils/db'
import {readConfigData} from '../../../../utils/config'

const path = require('path');
const util = require('util');
const exec = util.promisify(require('child_process').exec);
let configData = null

async function executeScript(patientId, studyDate, studyTime, folderName) {
    const uploadedDataPath = path.join(configData.uploadedDataPath, folderName)
    const pythonPath = path.join(configData.pythonPath, "contoursByPatient.py")
    const { stdout, stderr } = await exec(`python ${pythonPath} ${uploadedDataPath} ${patientId} ${studyDate} ${studyTime}`);
    return JSON.parse(stdout)
}

export default async function (req, res) {
    const { query: { id, studyDate, studyTime, folderName } } = req

    let folder = path.normalize(folderName)
    if(!folder || folder==="" || /[^A-Za-z0-9_\.]/.test(folder)) {
       res.status(400).json({message: "Folder name must only contain the characters A-Z, 0-9 and _"})
       return
     }

    const session = await checkSession(req, res)
    if(!session) {
        return
    }

    configData = readConfigData()

    const username = session.username
    switch (req.method) {
        case 'GET':
            try {
                const okget = await checkAccess(username, "getPatientContours", null)
                if (!okget) {
                    res.status(401).json({message: "Access failed"})
                    return
                }
                const data = await executeScript(id, studyDate, studyTime, folder)
                res.status(200).send(JSON.stringify(data))
            } catch (error) {
                console.error(error)
                res.status(500).json({ message: "Get the contours of the patient failed" })
            }
            break
        default:
            res.setHeader('Allow', ['GET'])
            res.status(405).json({message: `Method ${req.method} Not Allowed`})
    }
}