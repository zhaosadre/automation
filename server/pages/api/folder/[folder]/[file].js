import { checkAccess } from '../../../../utils/db'
import { checkSession } from '../../../../utils/session'
import {readConfigData} from '../../../../utils/config'

const fs = require('fs')
const path = require('path')

/**
 * DELETE /api/folder/[folder]/[file]
 */

 export default async function deleteDcmFile(req, res) {
     const { query: { folder, file } } = req

     const session = await checkSession(req, res)
     if (!session) {
         return
     }

     let dirname = folder.trim()
     if(!dirname || dirname==="" || /[^A-Za-z0-9_]/.test(dirname)) {
       res.status(400).send(JSON.stringify("Folder name must only contain the characters A-Z, 0-9 and _"))
       return
     }

     let filename = path.normalize(file)
     if(!filename || filename==="" || /[^A-Za-z0-9_\.]/.test(filename)) {
        res.status(400).send(JSON.stringify("File name must only contain the characters A-Z, 0-9 and _"))
        return
      }

     const username = session.username
     switch(req.method) {
        case 'DELETE':
            const okdelete = await checkAccess(username, "deleteDcmFile", null)
            if (!okdelete) {
                res.status(401).json({message: "Access failed"})
                return
            }
            let configData = readConfigData()
            const filePath = path.join(configData.uploadedDataPath, dirname, filename)
            fs.unlinkSync(filePath)
            res.status(200).send(JSON.stringify("OK"))
            break
        default:
            res.setHeader('Allow', ['DELETE'])
            res.status(405).json({message: `Method ${req.method} Not Allowed`})
     }  
 }