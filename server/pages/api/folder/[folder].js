import { checkAccess, insertFolder, deleteFolder, getAllFolders } from '../../../utils/db'
import { checkSession } from '../../../utils/session'
import { getCurrentDate} from '../../../utils/help'
import {readConfigData} from '../../../utils/config'

export const config = {
  api: {
    bodyParser: false
  }
}

const multer = require("multer");
const fs = require('fs')
const path = require('path');
const util = require('util');
const exec = util.promisify(require('child_process').exec);
let configData=null

async function executeScript(folder) {
  const folderPath = path.join(configData.uploadedDataPath, folder) 
  const pythonPath = path.join(configData.pythonPath, "getDcmfilesByFoldername.py")
  const { stdout, stderr } = await exec(`python ${pythonPath} ${folderPath}`);
  return JSON.parse(stdout)
}

/**
 * POST /api/folder/[folder]
 * GET /api/folder/[folder]
 * DELETE /api/folder/[folder]
 */
export default async function managerFolders(req, res){
    const { query: { folder } } = req

    const session = await checkSession(req, res)
    if(!session) {
        return
    }

    let dirname = folder.trim()
    if(!dirname || dirname==="" || /[^A-Za-z0-9_]/.test(dirname)) {
      res.status(400).json({message: "Folder name must only contain the characters A-Z, 0-9 and _"})
      return
    }

    configData = readConfigData()

    const username = session.username
    switch (req.method){
        case 'GET':
          try{
            const okget = await checkAccess(username, "getAllDcmFilesInFolder", dirname)
            if(!okget){
              res.status(401).json({message: "Access failed"})
              return
            }
            let data = await executeScript(dirname)
            res.status(200).send(JSON.stringify(data))
            return
          } catch (error){
            console.error(error)
            res.status(500).json({message: "Get all DCM filers in the folder failed."})
          }
          break
        case 'POST':
            try {
                const ok = await checkAccess(username, "uploadFile", null)
                if (!ok) {
                  res.status(401).json({message: "Access failed"})
                  return
                }
                
                let newDate = getCurrentDate().toString()
                const foldersInServer = await getAllFolders()
                if (!foldersInServer.find((f)=>f.folderName === dirname)){
                  await insertFolder(folder, username, newDate)
                }
                  
                dirname = path.join(configData.uploadedDataPath, dirname)

                const storage = multer.diskStorage({
                  destination: dirname,
                  filename: (req, file, cb) => {
                    cb(null, file.originalname)
                  }
                });
                const multerUpload = multer({ storage: storage })
                multerUpload.any()(req, {}, (err) => {
                  if(err) {
                    console.error(err.message)
                    res.status(500).json({message: "Upload files failed."})
                  }
                })

                res.status(200).end()
            } catch (error) {
                console.error(error)
                res.status(500).json({message: "Upload files failed."})
            }
            break
        case 'DELETE':
          const okdelete = await checkAccess(username, "deleteFolder", dirname)
          if(!okdelete) {
            res.status(401).json({message: "Access failed"})
            return
          }
          const filePath = path.join(configData.uploadedDataPath, dirname)
          fs.rmdirSync(filePath, { recursive: true });
          res.status(200).send(JSON.stringify("OK"))
          
          // delete the record from database
          await deleteFolder(dirname)
          break            
        default:
            res.setHeader('Allow', ['GET', 'POST', 'DELETE'])
            res.status(405).json({ message: `Method ${req.method} Not Allowed` })
    }
}