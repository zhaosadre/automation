import { checkSession } from '../../../utils/session'
import { checkAccess, addPublicKey } from '../../../utils/db'

/**
 * POST /api/admin/cleanunsafe
 */

export default async function(req, res) {
    const { query: { } } = req

    const session = await checkSession(req,res)
    if(!session) {
      return
    }

    const username = session.username
    switch (req.method) {
      case 'POST':
        const ok = await checkAccess(username, "publicKey", null)
        if (!ok) {
          res.status(401).json({message: "Access failed"})
          return
        }
        await addPublicKey(req.query.publicKey)
        res.status(200).send(JSON.stringify("OK"))
        break
      default:
        res.setHeader('Allow', ['POST'])
        res.status(405).json({message: `Method ${req.method} Not Allowed`})
    }
  }