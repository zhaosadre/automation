import { createUser } from '../../../lib/user'
import { checkAccess } from '../../../utils/db'
import { checkSession } from '../../../utils/session'

/**
 * 
 * POST /api/admin/signup
 */
export default async function signup(req, res) {
  const session = await checkSession(req,res)
  if(!session) {
    return
  }

  const username = session.username
  switch (req.method) {
    case 'POST':
      try {
        const ok = await checkAccess(username, "insertUser", null)
        if (!ok) {
          res.status(401).json({message: "Access failed"})
          return
        }
        await createUser(req.body)
        res.status(200).send({ done: true })
        break
      }
      catch (error) {
        console.error(error)
        res.status(500).json({message: error.message})
      }
      break
    default:
      res.setHeader('Allow', ['POST'])
      res.status(405).json({message: `Method ${req.method} Not Allowed`})
  }
}
