import { checkSession } from '../../../utils/session'
import { checkAccess, cleanUnsafe } from '../../../utils/db'

/**
 * POST /api/admin/cleanunsafe
 */

export default async function(req, res) {
    const { query: { } } = req

    const session = await checkSession(req,res)
    if(!session) {
      return
    }

    const username = session.username
    switch (req.method) {
      case 'PUT':
        const ok = await checkAccess(username, "clean", null)
        if (!ok) {
          res.status(401).json({message: "Access failed"})
          return
        }
        let count = await cleanUnsafe()
        res.status(200).send(JSON.stringify(count))
        break
      default:
        res.setHeader('Allow', ['PUT'])
        res.status(405).json({message: `Method ${req.method} Not Allowed`})
    }
  }