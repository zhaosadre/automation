import { checkAccess } from '../../utils/db'
import { checkSession } from '../../utils/session'
import {readConfigData} from '../../utils/config'

const util = require('util');
const exec = util.promisify(require('child_process').exec);
const path = require('path')
let configData = null

async function executeScript() {
    const pythonPath = path.join(configData.pythonPath, "getServerFolders.py")
    const serverDataPath = configData.uploadedDataPath
    const { stdout, stderr } = await exec(`python \"${pythonPath}\" \"${serverDataPath}\"`)
    return JSON.parse(stdout)
}

/**
 * GET /api/folders
 */
export default async function managerFolders(req, res){
    const { query: { } } = req

    const session = await checkSession(req, res)
    if(!session) {
        return
    }
    configData=readConfigData()

    const username = session.username
    switch(req.method){
        case 'GET':
            try {
                const ok = await checkAccess(username, "getFolder", null)
                if (!ok){
                    res.status(401).json({ message: "Access failed" })
                    return
                }
                const folders = await executeScript()
                res.status(200).send(JSON.stringify(folders))
            } catch (error) {
                console.error(error)
                res.status(500).json({ message: "Get server folder failed." })
            }
            break
        default:
            res.setHeader('Allow', ['GET'])
            res.status(405).json({ message: `Method ${req.method} Not Allowed` })
    }
}