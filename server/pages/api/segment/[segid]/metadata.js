import { checkSession } from '../../../../utils/session'
import { getSegMetaData, setSegMetaData, checkAccess } from '../../../../utils/db'

/**
 * GET /api/segment/[segid]/metadata
 * POST /api/segment/[segid]/metadata
 */
export default async function(req, res) {
    const { query: { segid } } = req

    const session = await checkSession(req,res)
    if(!session) {
      return
    }

    const username = session.username
    switch (req.method) {
      case 'GET':
        const okget = await checkAccess(username, "seg", segid)
        if (!okget) {
          res.status(401).json({message: "Access failed"})
          return
        }
        const data = await getSegMetaData(segid)
        res.status(200).send(JSON.stringify(data))
        break
      case 'PUT':
        const okpost = await checkAccess(username, "seg", segid)
        if (!okpost) {
            res.status(401).json({message: "Access failed"});
            return;
        }
        if(segid!=req.body.metaData.segid) {
            res.status(401).json({message: "segid in metadata does not match URI"});
        }
        switch(req.body.metaData.Modality) {
            case 'RTSTRUCT':
                await setSegMetaData(req.body.metaData)
                res.status(200).send({ done: true })
                break;
            default:
                res.status(400).json({message: "Modality wrong. Data ignored."});
        }
      default:
        res.setHeader('Allow', ['GET', 'PUT'])
        res.status(405).json({message: `Method ${req.method} Not Allowed`})
    }
  }