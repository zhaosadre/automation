import { checkSession } from '../../../../../utils/session'
import { checkAccess, deleteWorkflowUser } from '../../../../../utils/db'

/**
 * POST /api/workflow/[id]/users/[user]
 */

export default async function(req, res) {
    const { query: { id, user } } = req

    const session = await checkSession(req, res)
    if (!session) {
        return
    }

    let dirname = path.normalize(user)
    if(!dirname || dirname==="" || /[^A-Za-z0-9_\.]/.test(dirname)) {
       res.status(400).send(JSON.stringify("User name must only contain the characters A-Z, 0-9 and _"))
       return
     }

    const username = session.username
    switch(req.method) {
        case 'PUT':
            const okpost = await checkAccess(username, "modifyWorkflowUser", id)
            if (!okpost) {
                res.status(401).json({message: "Access failed"})
                return
            }

            // TO DO: need the serialID, Not finish yet!!!
            seriesId = null
            //await modifyWorkflowUser(id, seriesId, user)
            res.status(200).send(JSON.stringify("OK"))
            break
        case 'DELETE':
            const okdelete = await checkAccess(username, "deleteWorkflowUser", null)
            if (!okdelete) {
                res.status(401).json({message: "Access failed"})
                return
            }
            await deleteWorkflowUser(id, dirname)
            res.status(200).send(JSON.stringify("Successfully"))
            break
        default:
            res.setHeader('Allow', ['DELETE', 'PUT'])
            res.status(405).json({message: `Method ${req.method} Not Allowed`})
        }
}