import { checkSession } from '../../../utils/session'
import { getWorkflow, checkAccess, deleteWorkflow, getSeriesId} from '../../../utils/db'
import { readConfigData } from '../../../utils/config'
const fs = require('fs')
const path = require('path');

/**
 * GET /api/workflow/[id]
 * DELETE /api/workflow/[id]
 */

export default async function(req, res) {
    const { query: { id } } = req

    const session = await checkSession(req,res)
    if(!session) {
      return
    }

    const config = readConfigData()

    const username = session.username
    switch (req.method) {
      case 'GET':
        const okget = await checkAccess(username, "workflow", id)
        if (!okget) {
          res.status(401).json({message: "Access failed"})
          return
        }
        const data = await getWorkflow(id)
        res.status(200).send(JSON.stringify(data))
        break
      case 'DELETE':
        const seriesId = await getSeriesId(id)
        const filePath = path.join(config.imageDataPath, seriesId)
        const okdelete = await checkAccess(username, "deleteWorkflow", id)
        if (!okdelete) {
          return res.status(401).json({message: "Access failed"})
        }
        await deleteWorkflow(id)
        fs.rmdirSync(filePath, { recursive: true });
        res.status(200).send(JSON.stringify("Successful"))
        break
      default:
        res.setHeader('Allow', ['GET', 'DELETE'])
        res.status(405).json({message: `Method ${req.method} Not Allowed`})
    }
  }