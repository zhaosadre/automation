import { checkSession } from '../../../../../utils/session'
import { checkAccess } from '../../../../../utils/db'
import {readConfigData} from '../../../../../utils/config'

const util = require('util')
const path = require('path');
const exec = util.promisify(require('child_process').exec)
let configData = null

async function executeScript(foldername, ctFileName, rsFileName, contours, patientId, studyDate, studyTime, wfDescription, wfLabel) {
    const pythonPath = path.join(configData.pythonPath, "createWorkflow.py")
    const ctSeriesFilePath = path.join(configData.uploadedDataPath, foldername, ctFileName)
    const rsSegFilePath = path.join(configData.uploadedDataPath, foldername, rsFileName)

    const contourNumbers=JSON.parse(contours).map( (c)=> c.roinumber).join()
    // console.log(`python ${pythonPath} ${ctSeriesFilePath} ${rsSegFilePath} ${contourNumbers} ${patientId} ${studyDate} ${studyTime} ${wfDescription} ${wfLabel}`)
    const { stdout, stderr } = await exec(`python \"${pythonPath}\" \"${ctSeriesFilePath}\" \"${rsSegFilePath}\" ${contourNumbers} ${patientId} ${studyDate} ${studyTime} \"${wfDescription}\" \"${wfLabel}\"`)
    return JSON.parse(stdout) 
}

export default async function (req, res) {
    const { query: { folderName, ctFileName, rsFileName, contours, patientId, studyDate, studyTime, wfDescription, wfLabel} } = req
    
    configData = readConfigData()
    const session = await checkSession(req, res)
    if(!session) {
        return
    }

    let dirFolder = path.normalize(folderName)
    if(!dirFolder || dirFolder==="" || /[^A-Za-z0-9_\.]/.test(dirFolder)) {
       res.status(400).json({message: "Folder name must only contain the characters A-Z, 0-9 and _"})
       return
     }

    let dirCtName = path.normalize(ctFileName)
    if(!dirCtName || dirCtName==="" || /[^A-Za-z0-9_\.]/.test(dirCtName)) {
       res.status(400).json({message: "CT file name must only contain the characters A-Z, 0-9 and _"})
       return
     }

     let dirRSName = path.normalize(rsFileName)
     if(!dirRSName || dirRSName==="" || /[^A-Za-z0-9_\.]/.test(dirRSName)) {
        res.status(400).json({message: "RTSTRUCT file name must only contain the characters A-Z, 0-9 and _"})
        return
    }
    
    const username = session.username
    switch (req.method) {
        case 'POST':
            try {
                const okpost = await checkAccess(username, "newPatientWorkflow", null)
                if(!okpost) {
                    res.status(401).json({message: "Access failed"})
                    return
                }

                const data = await executeScript(dirFolder, dirCtName, dirRSName, contours, patientId, studyDate, studyTime, wfDescription, wfLabel)
                res.status(data.status).send(JSON.stringify(data))
            } catch (error) {
                console.error(error)
                res.status(500).json({ message: "Create the new workflow failed." })
            }
            break
        default:
            res.setHeader('Allow', ['POST'])
            res.status(405).json({message: `Method ${req.method} Not Allowed`})
    }
}