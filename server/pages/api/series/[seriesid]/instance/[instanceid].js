import { checkSession } from '../../../../../utils/session'
import { getSeriesDataByInstanceId, checkAccess } from '../../../../../utils/db'

/**
 * GET /api/series/[seriesid]/instance/[instanceid]
 */

export default async function(req, res) {
    const { query: { seriesid, instanceid } } = req

    const session = await checkSession(req,res)
    if(!session) {
      return
    }

    const username = session.username
    switch (req.method) {
      case 'GET':
        const okget = await checkAccess(username, "series", seriesid)
        if (!okget) {
          res.status(401).json({message: "Access failed"})
          return
        }
        const data = await getSeriesDataByInstanceId(seriesid,instanceid, "InstanceId")
        res.status(200).send(Buffer.from(data).fill(data))
        break
      default:
        res.setHeader('Allow', ['GET'])
        res.status(405).json({message: `Method ${req.method} Not Allowed`})
    }
  }