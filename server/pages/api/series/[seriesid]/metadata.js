import { checkSession } from '../../../../utils/session'
import { getSeriesMetaData, checkAccess } from '../../../../utils/db'

/**
 * GET /api/series/[seriesid]/metadata
 */

export default async function(req, res) {
    const { query: { seriesid } } = req

    const session = await checkSession(req,res)
    if(!session) {
      return
    }

    const username = session.username
    switch (req.method) {
      case 'GET':
        const okget = await checkAccess(username, "series", seriesid)
        if (!okget) {
          res.status(401).json({message: "Access failed"})
          return
        }
        const data = await getSeriesMetaData(seriesid)
        res.status(200).send(JSON.stringify(data))
        break
      default:
        res.setHeader('Allow', ['GET'])
        res.status(405).json({message: `Method ${req.method} Not Allowed`})
    }
  }