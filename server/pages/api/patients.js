import { checkSession } from '../../utils/session'
import { checkAccess } from '../../utils/db'
import { paths } from '../../utils/help'
import { readConfigData } from '../../utils/config'

const util = require('util');
const exec = util.promisify(require('child_process').exec);
const path = require('path')
let configData = null

async function executeScript() {
    const uploadedPath = configData.uploadedDataPath
    const pythonPath = path.join(configData.pythonPath, "patients.py")
    const { stdout, stderr } = await exec(`python \"${pythonPath}\" \"${uploadedPath}\"`);
    return JSON.parse(stdout)
}

/**
 * 
 * GET /api/patients
 */
export default async function(req, res) {
    const { query: { } } = req

    const session = await checkSession(req, res)
    if (!session) {
        return
    }
    configData = readConfigData()
    const username = session.username
    switch (req.method) {
        case 'GET':
            try {
                const okget = await checkAccess(username, "allPatients", null)
                if (!okget) {
                    res.status(401).json({message: "Access failed"})
                    return
                }
                const data = await executeScript()
                res.status(200).send(JSON.stringify(data))
            } catch (error) {
                console.error(error)
                res.status(500).json({ message: "Get patients failed." })
            }
            break
        default:
            res.setHeader('Allow', ['GET'])
            res.status(405).json({message: `Method ${req.method} Not Allowed`})
    }
}