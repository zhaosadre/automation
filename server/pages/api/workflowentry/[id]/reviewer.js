import { checkSession } from '../../../../utils/session'
import { checkAccess, addReviewerToWorkflow } from '../../../../utils/db'
const path = require('path');

/**
 * PUT /api/workflowentry/[id]/reviewer?user=[user]
 */

 export default async function (req, res) {
     const { query: {id, user} } = req

     const session = await checkSession(req, res)
     if (!session) {
         return
     }

     let dirUser = path.normalize(user)
     if(!dirUser || dirUser==="" || /[^A-Za-z0-9_\.]/.test(dirUser)) {
        res.status(400).json({message: "User name must only contain the characters A-Z, 0-9 and _"})
        return
      }

     const username = session.username
     switch (req.method) {
        case 'POST':
            const okput = await checkAccess(username, "addWorkflowEntry", id)
            if (!okput) {
                res.status(401).json({message: "Access failed"})
                return
            } 
            addReviewerToWorkflow(id, dirUser)
            res.status(200).send(JSON.stringify("OK"))
            break
        default:
            res.setHeader('Allow', ['POST'])
            res.status(405).json({message: `Method ${req.method} Not Allowed`})
         
     }
 }