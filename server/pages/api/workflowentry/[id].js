import { checkSession } from '../../../utils/session'
import { getWorkflowEntryData, checkAccess, deleteReviewer } from '../../../utils/db'

/**
 * GET    /api/workflowentry/[id]
 * DELETE /api/workflowentry/[id]
 */

export default async function(req, res) {
    const { query: { id } } = req

    const session = await checkSession(req,res)
    if(!session) {
      return
    }

    const username = session.username
    switch (req.method) {
      case 'GET':
        const okget = await checkAccess(username, "workflow", id)
        if (!okget) {
          res.status(401).json({message: "Access failed"})
          return
        }
        const data = await getWorkflowEntryData(id)
        res.status(200).send(JSON.stringify(data))
        break
      case 'DELETE':
          const okdelete = await checkAccess(username, "deleteReviewer", id)
          if (!okdelete){
              res.status(401).json({message: "Access failed"})
              return
          }
          await deleteReviewer(id)
          res.status(200).send(JSON.stringify(""))
          break
      default:
        res.setHeader('Allow', ['GET', 'DELETE'])
        res.status(405).json({message: `Method ${req.method} Not Allowed`})
    }
  }