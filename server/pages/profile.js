import { useUser } from '../lib/hooks'
import Layout from '../components/layout'
import { connectToDatabase, getUser } from '../utils/db'
import { getSession } from '../lib/iron'

export default function Profile ({ isConnected, admin, username, userrole}) {
  const user = useUser({ redirectTo: '/login' })

  let loginUserName = username
  if (user) {
    loginUserName = user.username
  }

  let createTime = null
  if (user) {
    createTime = new Date(user.createdAt).toISOString()
  }

  return (
    <Layout admin={admin}>
      <h1>Profile</h1>
      <h2 className="profileheadertext">Your Session</h2>
        <div className="profileList">
          <table className="profileTable">
            <thead>
              <tr>
                <th>UserName</th>
                <th>Role</th>
                <th>Login time</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>{loginUserName}</td>
                <td>{userrole}</td>
                <td>{createTime}</td>
              </tr>
            </tbody>
          </table>
        </div>
    </Layout>
  )
}

export async function getServerSideProps(context) {
  const { client } = await connectToDatabase()
  
  const isConnected = await client.isConnected() // Returns true or false
  
  const session = await getSession(context.req)
  
  let user = null
  let username = ""
  let userrole = ""
  if (session)
    user = await getUser(session.username)
    username = user.username
    userrole = user.role
    
  let admin = false
  if (user)
    admin = user.role=="admin"

  return {
    props: { isConnected, admin, username, userrole},
  }
}

