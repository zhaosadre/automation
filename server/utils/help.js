const path = require('path');

export const getCurrentDate = function (separator=''){
  let newDate = new Date()
  let date = newDate.getDate()
  let month = newDate.getMonth() + 1
  let year = newDate.getFullYear()
  return `${year}${separator}${month<10?`0${month}`:`${month}`}${separator}${date}`
}

export const openPanel = function (ids, document) {
  if (!ids || !ids.length){
    return
  }
  closeAllPanels(document)
  for (let i=0; i<ids.length; i++){
    document.getElementById(ids[i]).removeAttribute("hidden")
  }
  removeCheckItems(document)
}

export const closePanelOnly = function(ids, document) {
  if (!ids || !ids.length){
    return
  }
  for (let i=0; i<ids.length; i++){
    document.getElementById(ids[i]).setAttribute("hidden", "")
  }
}

export const closePanel = function(ids, document) {
  if (!ids || !ids.length){
    return
  }
  for (let i=0; i<ids.length; i++){
    document.getElementById(ids[i]).setAttribute("hidden", "")
  }
  emptyInputFields(document)
  removeCheckItems(document)
}

export const closeAllPanels = function(document) {
  if(document.getElementById("uploadDivId")) {
    document.getElementById("uploadDivId").setAttribute("hidden", "")
  }
  if(document.getElementById("assignUsersWfId")) {
    document.getElementById("assignUsersWfId").setAttribute("hidden", "")
  }
  if(document.getElementById("patientsDivId")) {
    document.getElementById("patientsDivId").setAttribute("hidden", "")
  }
  if(document.getElementById("patientContourspanelId")) {
    document.getElementById("patientContourspanelId").setAttribute("hidden", "")
  }
  if(document.getElementById("workflowGraphId")) {
    document.getElementById("workflowGraphId").setAttribute("hidden", "")
  }
  if(document.getElementById("uploadDivId")) {
    document.getElementById("uploadDivId").setAttribute("hidden", "")
  }
  if (document.getElementById("dcmDataInFolderDivId")) {
    document.getElementById("dcmDataInFolderDivId").setAttribute("hidden", "")
  }
  if (document.getElementById("createProgressDivId")) {
    document.getElementById("createProgressDivId").setAttribute("hidden", "")
  }
  if (document.getElementById("dcmDataInFolderDivId")) {
    document.getElementById("dcmDataInFolderDivId").setAttribute("hidden", "")
  }

  if (document.getElementById("cleanUnrecordFolderId")) {
    document.getElementById("cleanUnrecordFolderId").setAttribute("hidden", "")
  }

  emptyInputFields(document)
  removeCheckItems(document)
}

export const emptyInputFields = function(document) {
  if (document.getElementById("wfLabelId")) {
    document.getElementById("wfLabelId").value ="InitialStage"
  }
  
  if (document.getElementById("wfDescriptionId")) {
    document.getElementById("wfDescriptionId").value ="Workflow"
  }

  if (document.getElementById("uploadFolderNameId")){
    document.getElementById("uploadFolderNameId").value=""
  }
}

export const removeCheckItems = function(document) {
  const res = document.getElementsByTagName("input")
  if (res || res.length) {
    for(let i=0; i<res.length; i++ ) {
      res[i].checked = false
    }
  }
}

export const closeThenOpenPanel = function(closePanelId, openPanelId, document) {
  if (closePanelId && document) {
    document.getElementById(closePanelId).setAttribute("hidden", "")
  }
  if (openPanelId && document) {
    document.getElementById(openPanelId).removeAttribute("hidden")
  }
  removeCheckItems(document)
}

export const toggleAll = function(document, tasks) {
  const selectAll = document.getElementById("selectAll").checked;
  
  for (let i=0; i<tasks.length; i++)
    document.getElementById("workflowcheckbox"+tasks[i].workflowId).checked = selectAll;
}

export const toggleAllFolders = function(document, folders) {
  const selectAll = document.getElementById("selectAll").checked;
  
  for (let i=0; i<folders.length; i++)
    document.getElementById("foldercheckbox"+folders[i].folderName).checked = selectAll;
}

export const getNumberOfSelectedWorkflows = function(tasks, document) {
  let count=0
  for (let i=0; i<tasks.length; i++) {
    if (document.getElementById("workflowcheckbox"+tasks[i].workflowId).checked) {
      count++
    }
  }
  return count
}

export const getSelectedUsers = function(users, document) {
  let selectedUsers=[];
  for (let i=0; i<users.length; i++) {
   if (document.getElementById("usercheckbox"+users[i].username).checked) {
      selectedUsers.push(users[i].username)
    }
  }
  return selectedUsers
}

export const showUserSelectionDialog = function(users, document) {
  // Uncheck all checkboxes
  for (let i=0; i<users.length; i++) {
    document.getElementById("usercheckbox"+users[i].username).checked=false
  }
  // Show users list
  openPanel(["assignUsersWfId"], document)
}


export const getSeletedPatient = function(patients, document){
  let patient = null
  for (let i=0; i<patients.length; i++) {
    if (document.getElementById("patientradio"+patients[i].name).checked) {
      patient = patients[i]
    }
  }
  return patient
}

export const getSelectedFileName = function(data, document, prefClassId, modality=""){
  let res=[]
  for (let i=0; i<data.length; i++) {
    if (document.getElementById(prefClassId+data[i].fileName).checked) {
      if(data[i]["modality"]==modality){
        res.push(data[i])
      } else if (!modality){
        res.push(data[i])
      }
    }
  }
  return res
}

export const getSeletedFolders = function (folders, document) {
  let result = []
  if (folders && folders.length){
      for (let i=0; i<folders.length; i++){
          if (document.getElementById("foldercheckbox"+folders[i].folderName)) {
              if (document.getElementById("foldercheckbox"+folders[i].folderName).checked){
                  result.push(folders[i].folderName)
              }
          }
      }
      return result
  }
}

export const getSeletedWorkflows = function (wfs, document) {
  let result = []
  if (wfs && wfs.length){
      for (let i=0; i<wfs.length; i++){
          if (document.getElementById("workflowcheckbox"+wfs[i].workflowId)) {
              if (document.getElementById("workflowcheckbox"+wfs[i].workflowId).checked){
                  result.push(wfs[i])
              }
          }
      }
      return result
  }
}

export const getCheckedDcmFiles = function(dcmFiles, document){
  let res = []
  if (dcmFiles && dcmFiles.length){
    for (let i=0; i<dcmFiles.length; i++){
      if (document.getElementById("filenameCheckbox"+dcmFiles[i].fileName).checked){
        res.push(dcmFiles[i])
      }
    }
  }
  return res
}


export const getSelectedContours = function(contours, document){
  let result = []
  for (let i=0; i<contours.length; i++){
    if (document.getElementById("contourcheckbox"+contours[i].roiname).checked) {
      result.push(contours[i])
    }
  }
  return result
}

export const getEntryText = function(classId, document, isDescription) {
  let res=""
  if (document.getElementById(classId)) {
    let text = document.getElementById(classId).value
    if (!text && !text.length){
      if (isDescription) {
        res = "No description"
      } else {
        res = "InitialStage"
      }
    } else {
      res = text
    } 

  }
  return res.trim()
}

export const workflowToData = function (workflow) {
  let parent = workflow[0].parents[0];
  
  let x=0, y=-100, data=[];
    
  for (let i=0; i<workflow.length; i++){
    const id = workflow[i]._id.toString();
    
    if (workflow[i].parents[0] != parent) {
      x += 100;
      y = 0;
    } else {
      y += 100;
    }
    
    const name = workflow[i].label;
    
    data.push({name, id, x, y});
    
    parent = workflow[i].parents[0];
  }
  
  return data;
}

export const workflowToLinks = function (workflow) {
  let links = [];
  const symbolSize = [5, 20];
  const label = {show: false};
  const lineStyle = { width: 5, curveness: 0.2, color: "green"};
  
  for (let i=0; i<workflow.length; i++){
    let target = workflow[i]._id.toString();
    
    for (let j=0; j<workflow[i].parents.length; j++) {
      let source = workflow[i].parents[j].toString();
      
      links.push({source, target, label, symbolSize, lineStyle});
    
    }
  }
  return links;
}

export const isStringValid = function(text){
  if(!text){
    return false
  }
  text = path.normalize(text)
  if(!text || text==="" || /[^A-Za-z0-9_\.]/.test(text)) {
    return false
  } else {
    return true
  }
}