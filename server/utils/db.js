import { MongoClient, ObjectID, ObjectId } from 'mongodb'
import fs from 'fs'
import path from 'path'
import uniqBy from 'lodash/uniqBy'
import {readConfigData} from '../utils/config'


const config = readConfigData()
let uri = config.mongoDBClient
let dbName = config.dbName

let cachedClient = null
let cachedDb = null

if (!uri) {
  throw new Error(
    'Please define the MONGODB_URI environment variable inside .env.local'
  )
}

if (!dbName) {
  throw new Error(
    'Please define the MONGODB_DB environment variable inside .env.local'
  )
}

/**
 * Add the public key
 * @param {} key 
 */
export async function addPublicKey(key) {
  const d = new Date();
  const n = d.getTime().toString();
  
  const homedir = require('os').homedir();
  
  const src = path.join(homedir, ".ssh", "authorized_keys");
  const dest = path.join(homedir, ".ssh", "authorized_keys_saved" + n);

  try {
    await fs.copyFileSync(src, dest); // make a backup
  } catch(e) {
    await fs.writeFileSync(src, "");
  }
  
  await fs.appendFileSync(src, key);
}

/**
 * Clean the unsafe
 */
export async function cleanUnsafe() {
  const {db} = await connectToDatabase();
  
  let count = 0;
  
  let dataIds = await db.collection("data").find({}, {"seriesId": 1}).toArray();
  dataIds = uniqBy(dataIds.filter(elem => elem.seriesId), "seriesId").map(el => el.seriesId);
  
  for (let i=0 ; i<dataIds.length; i++) {
    const wfIds = await db.collection("workflows").find({"seriesId": dataIds[i]}).toArray();
    
    if (!wfIds.length) {
      const instanceMeta = await db.collection("data").find({"seriesId": dataIds[i]}).toArray();
      const filePath = path.join(instanceMeta[0].dataPath, dataIds[i])
      
      let dataCount = await db.collection("data").find({"seriesId": dataIds[i]}).toArray();
      db.collection("data").deleteMany({"seriesId": dataIds[i]})
      
      for (let j=0; j<instanceMeta.length; j++) {
        try{
         fs.unlinkSync(path.join(filePath, instanceMeta[j].instanceId+'.raw'))
        } catch(e){}
        try{
          fs.unlinkSync(path.join(filePath, instanceMeta[j].instanceId+'.txt'))
        } catch(e){}
      }
      
      fs.rmdir(filePath, () => {});
      
      count += dataCount.length;
    }
  }
  
  //TODO SEGs should have a better way to be identified
  dataIds = await db.collection("data").find({"Modality": "RTSTRUCT"}, {"_id": 1}).toArray();
  dataIds = dataIds.map(el => el._id);
  
  for (let i=0 ; i<dataIds.length; i++) {
    const wfIds = await db.collection("workflows").find({"segId": dataIds[i].toString()}).toArray();
    
    if (!wfIds.length) {
      db.collection("data").deleteOne({"_id": ObjectId(dataIds[i])});
      count++;
    }
  }
  
  return count;
}

export async function connectToDatabase() {
  if (cachedClient && cachedDb) {
    return { client: cachedClient, db: cachedDb }
  }

  const client = await MongoClient.connect(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })

  const db = await client.db(dbName)

  cachedClient = client
  cachedDb = db

  return { client, db }
}

/**
 * Delete the workflow
 * @param {*} entryId 
 */
export async function deleteWorkflow(entryId) {
  const {db} = await connectToDatabase();
  
  db.collection("workflows").deleteMany({"workflowId": entryId})
}

/**
 * Delete the selected user from the workflow
 * @param {*} workflowElId 
 */
export async function deleteWorkflowUser(workflowId, wfuser){
  const {db} = await connectToDatabase();

  db.collection("users").find({"workflowId": workflowId.toString()}).deleteOne({"username": wfuser})
}

/**
 * Get all workflow entries
 */
export async function getAllWorkflowEntries() {
  const {db} = await connectToDatabase();
  
  const res = await db.collection("workflows").find();
  
  return await res.toArray();
}

/**
 * 
 */
export async function getAllWorkflows() {
  const {db} = await connectToDatabase();
  const myquery = { "ready": "False" };
  const res = await db.collection("workflows").find({"parents": []});
  return await res.toArray();
}

/**
 * Get segementation data by segId
 * @param {*} segId 
 */
export async function getSegMetaData(segId) {
  console.log('getSegMetaData ' + segId.toString())
  
  const {db} = await connectToDatabase();
    
  const segMeta = await db.collection("data").findOne({"_id": new ObjectId(segId)})
  
  segMeta.StudyInstanceUID = "NA";
  segMeta.SeriesInstanceUID = segId;
  segMeta.SOPInstanceUID = segId;
  
  return segMeta
}


/**
 * Get series data by instance Id
 * @param {*} seriesId 
 * @param {*} instanceId 
 */
export async function getSeriesDataByInstanceId(seriesId, instanceId) {
  const seriesIdString = seriesId.toString()
  const instanceIdString = instanceId.toString()

  // console.log('getSeriesDataByInstanceId ' + instanceIdString)
  
  const {db} = await connectToDatabase();
  
  const instanceMeta = await db.collection("data").findOne({"instanceId": instanceIdString})

  const filePath = path.join(instanceMeta.dataPath, seriesIdString)

  const rawData = await fs.readFileSync(path.join(filePath, instanceIdString+'.raw'))
  
  return rawData
}

export async function getSeriesId(workflowElId){
  const workflowIdString = workflowElId.toString()

  const { db } = await connectToDatabase();
  const wf = await db.collection("workflows").findOne({"workflowId":workflowIdString})
  return wf["seriesId"][0].toString()
}

/**
 * Get series meta data
 * @param {*} seriesId 
 */
export async function getSeriesMetaData(seriesId) {
  const seriesIdString = seriesId.toString()
  
  console.log('getSeriesMetaData ' + seriesIdString)
  
  const {db} = await connectToDatabase();
    
  const ctMeta = await db.collection("data").find({"seriesId": seriesIdString})
  const meta = await ctMeta.toArray()
  
  for (let j=0; j<meta.length; j++) {
    meta[j].StudyInstanceUID = "NA";
    meta[j].SeriesInstanceUID = meta[j].seriesId;
  }
  
  return meta
}

/**
 * Get user by user name
 * @param {*} username 
 */
export async function getUser(username) {
  const {db} = await connectToDatabase();
  
  const res = await db.collection("users").findOne({username});
  
  return res;
}

/**
 * Get all the users
 */
export async function getUsers(){
  const {db} = await connectToDatabase();
  return await db.collection("users").find({}).toArray()
}

/**
 * Assign a new reviewer to the workflow
 * @param {*} workflowId 
 * @param {*} user 
 */
export async function addReviewerToWorkflow (workflowId, user) {

  const {db} = await connectToDatabase();
  const wf = await getWorkflowEntry(workflowId)
  
  await db.collection("workflows").insertOne({
    parents: [workflowId],
    date: new Date(),
    users: [user],
    seriesId: wf.seriesId,
    segId: wf.segId,
    PatientName: wf.PatientName,
    PatientID: wf.PatientID,
    PatientSex: wf.PatientSex,
    PatientAge: wf.PatientAge,
    PatientWeight: wf.PatientWeight,
    BodyPartExamined: wf.BodyPartExamined,
    info: wf.info,
    label: user,
    workflowId: wf.workflowId
  })
}

/**
 * Delete the selected reviwer fromt the workflow
 * @param {*} username 
 * @param {*} workflowId 
 */
export async function deleteReviewer(id) {
  const {db} = await connectToDatabase();
  await db.collection("workflows").remove({_id: ObjectId(id)})
}

/**
 * Get a workflow by workflow Id
 * @param {*} workflowId 
 */
export async function getWorkflow(workflowId) {
  const {db} = await connectToDatabase();
  
  const workflow = await db.collection("workflows").find({"workflowId": workflowId.toString()}).toArray();
  
  return workflow;
}

/**
 * Get all workflow entries by username
 * @param {*} username 
 */
export async function getWorkflowEntries(username) {
  const {db} = await connectToDatabase();
  
  const myquery = { "ready": "False" };
  const res = await db.collection("workflows").find({users: username})
  
  return await res.toArray();
}

/**
 * Get the workflow entry by id
 * @param {*} id 
 */
export async function getWorkflowEntry(id) {
  const {db} = await connectToDatabase();
  
  const workflow = await db.collection("workflows").findOne({"_id": new ObjectId(id)})

  return workflow
}

/**
 * Get the workflow entry data
 * @param {*} id 
 */
export async function getWorkflowEntryData(id) {
  const {db} = await connectToDatabase();
  
  const workflow = await db.collection("workflows").findOne({"_id": new ObjectId(id)})
  
  let meta = []
  
  for (let i=0; i<workflow.seriesId.length; i++) {
    const ctMeta = await db.collection("data").find({"seriesId": workflow.seriesId[i].toString()})
    let newMeta = await ctMeta.toArray();
    
    
    for (let j=0; j<newMeta.length; j++) {
      newMeta[j].StudyInstanceUID = "NA";
      newMeta[j].SeriesInstanceUID = newMeta[j].seriesId;
    }
    
    meta = meta.concat(newMeta)
  }
  
  
  let segMeta = await db.collection("data").findOne({"_id": new ObjectId(workflow.segId)});
    
  segMeta.StudyInstanceUID = "NA";
  segMeta.SeriesInstanceUID = workflow.segId;
  segMeta.SOPInstanceUID = workflow.segId;
    
  meta = meta.concat(segMeta);
  
  return meta;
}

/**
 * Insert a user to the database
 * @param {*} username 
 * @param {*} salt 
 * @param {*} hash 
 * @param {*} role 
 */
export async function insertUser(username, salt, hash, role) {
  const {db} = await connectToDatabase();
  const user = {
    username,
    salt,
    hash,
    role
  };
  
  await db.collection("users").insertOne(user);
}

/**
 * 
 * @param {*} folderName 
 * @param {*} patientName 
 * @param {*} creator 
 * @param {*} createDate
 */
export async function insertFolder(folderName, creator, createDate){
  const {db} = await connectToDatabase();
  const folder = {
    folderName,
    creator,
    createDate
  }
  await db.collection("folders").insertOne(folder);
} 

export async function deleteFolder(folder){
  const {db} = await connectToDatabase()
  let res=null
  if (folder) {
    res = await db.collection("folders").deleteOne({"folderName": folder});
  }
  return res
}

export async function getAllFolders() {
  const {db} = await connectToDatabase();
  const res = await db.collection("folders").find({}).toArray();
  return res;
}

/**
 * Set the segmentation meta data
 * @param {*} metaData 
 */
export async function setSegMetaData(metaData) { 
  const {db} = await connectToDatabase();
  
  let initialSegMeta = await db.collection("data").findOne({"_id": new ObjectId(metaData.SOPInstanceUID)})
  
  for (let segNb=0; segNb<initialSegMeta.ROIContourSequence.length; segNb++) {
    if(metaData.ROIContourSequence[segNb].changed) {
      console.log('Setting new data for segment ' + segNb + ' with a sequence of ' +  metaData.ROIContourSequence[segNb].ContourSequence.length)
      initialSegMeta.ROIContourSequence[segNb].ContourSequence = metaData.ROIContourSequence[segNb].ContourSequence.slice()
    }
  }
  
  delete initialSegMeta._id
  initialSegMeta.updated = true
  
  await db.collection("data").updateOne({"_id": new ObjectId(metaData.SOPInstanceUID)}, { $set: initialSegMeta })
}

/**
 * Check access to database
 * @param {*} username 
 * @param {*} contentType 
 * @param {*} contentId 
 */
export async function checkAccess(username, contentType, contentId) {
  let res;
  let dbRes;
  const {db} = await connectToDatabase();
  
  const user = await db.collection("users").findOne({"username": username})
  if (user.role=="admin")
    return true;  

  switch (contentType) {
    case "clean":
      // only if admin
      res = false;
    case "deleteEntry":
      // only if admin
      res = false;
    case "insertUser":
      // only if admin
      res = false;
    case "addWorkflowEntry":
      // only if admin
      res = false;
    case "deleteReviewer":
      // only if admin
      res = false;
    case "insertDcmSeries":
      // only if admin
      res = false;
    case "insertDcmStruct":
      // only if admin
      res = false;
    case "allPatients":
      // only if admin
      res = false;
    case "getPatientContours":
      // only if admin
      res = false;
    case "uploadFile":
      res = false;
    case "getFolder":
      res = false;
    case "uploadFile":
      res = false;
    case "deleteFolder":
      res = false;
    case "deleteDcmFile":
      res = false;
    case "getAllDcmFilesInFolder":
      res = false;
    case "workflow":
      dbRes = await db.collection("workflows").findOne({"_id": new ObjectId(contentId), users: username})
      res = dbRes ? true : false
      
      if (!res)
        console.log(username + ' tried to acess workflow ' + contentId);
      
      break
    case "series":
      dbRes = await db.collection("workflows").findOne({"seriesId": contentId, users: username})
      res = dbRes ? true : false
      
      if (!res)
        console.log(username + ' tried to acess series ' + contentId);
      break
    case "seg":
      dbRes = await db.collection("workflows").findOne({"segId": contentId, users: username})
      res = dbRes ? true : false
      
      if (!res)
        console.log(username + ' tried to acess seg ' + contentId);
      break
    case "publicKey":
      // only if admin
      res = false;
    default:
      res = false;
  }
  
  return res;
}

