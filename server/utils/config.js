const fs = require('fs');
const yaml = require('js-yaml');

const dev = process.env.NODE_ENV !== 'production';
export const readConfigData = function(){
  let data = null
  try{
    if (dev) {
      let fileContents = fs.readFileSync('config/config_dev.yaml', 'utf8')
      data = yaml.load(fileContents)
    } else {
      let fileContents = fs.readFileSync('config/config_prod.yaml', 'utf8')
      data = yaml.load(fileContents)
    }
    return data
  } catch(e){
    console.log("error message", e);
  }
}