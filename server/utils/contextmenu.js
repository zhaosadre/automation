export const getContextMenuPosition = function(document, event) {
    var posx = 0;
    var posy = 0;
  
    if (!event) var event = window.event;
  
    if (event.pageX || event.pageY) {
      posx = event.pageX;
      posy = event.pageY;
    } else if (event.clientX || event.clientY) {
      posx = event.clientX + document.body.scrollLeft + 
                         document.documentElement.scrollLeft;
      posy = event.clientY + document.body.scrollTop + 
                         document.documentElement.scrollTop;
    }
  
    return {
      x: posx,
      y: posy
    }
  }