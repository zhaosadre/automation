import { confirmAlert } from 'react-confirm-alert'
import 'react-confirm-alert/src/react-confirm-alert.css'

export const charpAlert = function(title, message, onClickFunctionOk=()=>{}){
    return confirmAlert({
        title: title,
        message: message,
        buttons: [
            {
                label: "OK",
                onClick: onClickFunctionOk,
            },
        ],
        closeOnClickOutside: false,
        closeOnEscape:false
    });
  }

  export const charpAlertDupleButtons = function(title, message, onClickFunctionOk=()=>{}, onClickFunctionNo=()=>{}){
    return confirmAlert({
        title: title,
        message: message,
        buttons: [
            {
                label: "Yes",
                onClick: onClickFunctionOk
            },
            {
                label: "No",
                onClick: onClickFunctionNo
            }
        ],
        closeOnClickOutside: false,
        closeOnEscape:false
    });
  }