import { getSession } from '../lib/iron'

export async function checkSession(req,res) {
    try {
        const session = await getSession(req)
        if (!session) {
            res.status(401).json({ message: "User not authenticated" })
        }
        return session
    }
    catch (e) {
        res.status(401).json({ message: "Error" })
        return null
    }
}