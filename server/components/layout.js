import Head from 'next/head'
import Header from './header'

const Layout = (props) => {
  return(
  <>
    <div className="layoutMain">
      <Head>
        <title>CHARP</title>
      </Head>

      <Header admin={props.admin}/>

      <main>
        {props.children}
      </main>
    </div>
  </>)
}

export default Layout
