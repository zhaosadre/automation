
import React, { Component } from "react";
import Layout from './layout'
import Link from "next/link"

export default class AdminLayout extends React.Component {
  constructor(props) {
    super(props);
  }
  
  render() {
    return (
      <Layout admin={true}>
        <div className='mainBlock'>
          <nav>
            <ul>
              <li>
                <Link href={{ pathname: "/admin/manageFiles" }}>
                  <a>Manage files</a>
                </Link>
              </li>
              <li>
                <Link href={{ pathname: "/admin/manageWorkflows" }}>
                  <a>Manage workflows</a>
                </Link>
              </li>
              <li>
                <Link href={{ pathname: "/admin/addUser"}}>
                  <a>Add users</a>
                </Link>
              </li>
              <li>
                <Link href={{ pathname: "/admin/manageKeys"}}>
                  <a>SSH keys</a>
                </Link>
              </li>
            </ul>
          </nav>
          <div className="tabcontent">
              {this.props.children}
          </div>
        </div>
      </Layout>
    );
  }
}

