
import React, { Component } from "react";
import styled, { createGlobalStyle } from 'styled-components'
import * as echarts from "echarts";

export default class ManageWorkflows extends React.Component {
  constructor(props) {
    super(props);
    
    this.container = React.createRef();
    window.addEventListener("contextmenu", e=>e.preventDefault())
  }
  
  componentDidMount() {
    this.componentDidUpdate();
  }
  
  componentDidUpdate() {
    const option = {
        title: {
            text: 'Active workflow'
        },
        tooltip: {},
        animationDurationUpdate: 1500,
        animationEasingUpdate: 'quinticInOut',
        series: [
            {
                type: 'graph',
                layout: 'none',
                symbolSize: 50,
                roam: true,
                label: {
                    show: true
                },
                edgeSymbol: ['circle', 'arrow'],
                edgeSymbolSize: [4, 10],
                edgeLabel: {
                    fontSize: 20
                },
                data: [{
                    name: 'Data uploaded',
                    x: 300,
                    y: 300
                }, {
                    name: 'DL',
                    x: 400,
                    y: 300
                }, {
                    name: 'User 1',
                    x: 500,
                    y: 200
                }, {
                    name: 'User 2',
                    x: 500,
                    y: 300
                }
                , {
                    name: 'User 3',
                    x: 500,
                    y: 400
                }, {
                    name: 'STAPLE',
                    x: 600,
                    y: 300
                }
                ],
                // links: [],
                links: [{
                    source: 0,
                    target: 1,
                    symbolSize: [5, 20],
                    label: {
                        show: false
                    },
                    lineStyle: {
                        width: 5,
                        curveness: 0.2,
                        color: "green"
                    }
                },
                {
                    source: 1,
                    target: 2,
                    symbolSize: [5, 20],
                    label: {
                        show: false
                    },
                    lineStyle: {
                        width: 5,
                        curveness: 0.2,
                        color: "green"
                    }
                },
                {
                    source: 1,
                    target: 3,
                    symbolSize: [5, 20],
                    label: {
                        show: false
                    },
                    lineStyle: {
                        width: 5,
                        curveness: 0.2,
                        color: "green"
                    }
                },
                {
                    source: 1,
                    target: 4,
                    symbolSize: [5, 20],
                    label: {
                        show: false
                    },
                    lineStyle: {
                        width: 5,
                        curveness: 0.2,
                        color: "green"
                    }
                },
                {
                    source: 2,
                    target: 5,
                    symbolSize: [5, 20],
                    label: {
                        show: false
                    },
                    lineStyle: {
                        width: 5,
                        curveness: 0.2,
                        color: "gray"
                    }
                },
                {
                    source: 3,
                    target: 5,
                    symbolSize: [5, 20],
                    label: {
                        show: false
                    },
                    lineStyle: {
                        width: 5,
                        curveness: 0.2,
                        color: "gray"
                    }
                },
                {
                    source: 4,
                    target: 5,
                    symbolSize: [5, 20],
                    label: {
                        show: false
                    },
                    lineStyle: {
                        width: 5,
                        curveness: 0.2,
                        color: "gray"
                    }
                },
                ],
                lineStyle: {
                    opacity: 0.9,
                    width: 2,
                    curveness: 0
                }
            }
        ]
    };
    
    const myChart = echarts.init(document.getElementById("workflowId"));
    myChart.setOption(option);

  }
  
  render() {
    const style = { width: '720px', height: '720px', position: 'relative' };

    return (
      <div style={style} class="workflow" id="workflowId" ref={this.container} />
    );
  }
}

