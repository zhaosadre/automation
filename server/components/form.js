import Link from 'next/link'

const Form = ({ isLogin, errorMessage, onSubmit }) => (
  <form onSubmit={onSubmit}>
    <label>
      <span>Username</span>
      <input type="text" name="username" required />
    </label>
    <label>
      <span>Password</span>
      <input type="password" name="password" required />
    </label>
    {!isLogin && (<>
      <label>
        <span>Role</span>
        <select name="role" id="roles">
          <option value="admin">admin</option>
          <option value="contourer">contourer</option>
        </select>
      </label>
      <label>
        <span>Repeat password</span>
        <input type="password" name="rpassword" required />
      </label>
      </>
    )}

    <div className="submit">
      {isLogin ? (
        <>
          <button type="submit">Login</button>
        </>
      ) : (
        <>
          <button type="submit">Signup</button>
        </>
      )}
    </div>

    {errorMessage && <p className="error">{errorMessage}</p>}

  </form>
)

export default Form
