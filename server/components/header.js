import Link from 'next/link'
import { useUser } from '../lib/hooks'
import dynamic from "next/dynamic";

const Header = (props) => {
  const user = useUser()

  return (
    <header>
      <nav>
        <ul>
          <li>
            <Link href="/">
              <a>Home</a>
            </Link>
          </li>
          {user ? (
            <>
              <li>
                <Link href="/profile">
                  <a>Profile</a>
                </Link>
              </li>
              {props.admin ? (
                <li>
                  <Link href="/admin">
                    <a>Admin</a>
                  </Link>
                </li>
              ) : (null)}
              <li className="logoutli">
                <a href="/api/logout">You are currently logged in as: {user.username} > Logout</a>
              </li>
            </>
          ) : (
            <li>
              <Link href="/login">
                <a>Login</a>
              </Link>
            </li>
          )}
        </ul>
      </nav>
    </header>
  )
}

export default Header
