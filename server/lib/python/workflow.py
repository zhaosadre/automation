import os
import pymongo
from dcm import CTImage, Struct
from mongodb import convertMetaToCollection, writeImage, readImage
import time
import datetime
import numpy as np
import pydicom
from bson.objectid import ObjectId
from db import DB
from help import writeToFile, readConfigData
import sys

class Workflow:
    config = readConfigData()
    dataPath = config['imageDataPath']
    dbConnection = DB(dataPath=dataPath, dbName=config['dbName']).connection()
    sftp = DB(dataPath=dataPath, dbName=config['dbName']).getSftp()

    def deleteWorkflow(self, workflowId):
        workflows = self.dbConnection["workflows"]
        workflow = workflows.remove({'workflowId': str(workflowId)})

    def deleteWorkflowEntry(self, entryId):
        workflows = self.dbConnection["workflows"]
        workflow = workflows.removeOne({'_id': ObjectId(entryId)})
    
    def getRT(self, segId):
        data = self.dbConnection["data"]
        seg = data.find_one({'_id': ObjectId(segId)}, {"_id": 0})
        return seg

    def getSeries(self, seriesId):
        data = self.dbConnection["data"]
        
        seriesEntries = list(data.find({'seriesId': str(seriesId)}, {"_id": 0}).sort("sliceNb"))
        
        npImages = []
        
        for i, meta in enumerate(seriesEntries):
            print("Import slice " + str(i+1) + "/" + str(len(seriesEntries)), file=sys.stderr)
            
            sliceData = readImage(seriesEntries[0]["dataPath"], meta, sftp=self.sftp)
            npImages.append(sliceData)
            
            sliceData.reshape((seriesEntries[0]["Rows"], seriesEntries[0]["Columns"]))
        
        data = np.dstack(npImages).astype("float32")
        
        return {"meta": seriesEntries, "data": data}
    
    def getWorkflow(self, workflowId):
        workflows = self.dbConnection["workflows"]
        
        workflow = workflows.find({"workflowId": str(workflowId)})
        return list(workflow)
    
    def getWorkflowEntry(self, entryId):
        workflows = self.dbConnection["workflows"]
        workflow = workflows.find_one({'_id': ObjectId(entryId)}, {"_id": 0})
        return workflow
    
    def getWorkflowEntry(self, entryId):
        workflows = self.dbConnection["workflows"]
        workflow = workflows.find_one({'_id': ObjectId(entryId)}, {"_id": 0})
        return workflow
    
    # insert dcm series
    # Only one dcm file per series. ctList can contain several series
    def insertDcmSeries(self, ctList):
        if not isinstance(ctList, list):
            ctList = [ctList]
        
        data = self.dbConnection["data"]

        seriesIds = []
        ctMetas = []
        cts = []

        for _, ctFileName in enumerate(ctList):
            print("Import series " + str(ctFileName), file=sys.stderr)
            
            ctImage = CTImage()
            ctImage.loadCT(ctFileName)
            
            ctMeta = ctImage.getMeta()

            ct = ctImage.getNpImage()
            cts.append(ct)
            
            res = self.insertData(ctMeta, ct)
            seriesIds.append(res["id"])
            ctMetas.append(res["meta"])
            
        return {"id": seriesIds, "meta": ctMetas, "data": cts}

    # Insert the dcm RT struct file
    def insertDcmStruct(self, structFileName, ctMeta, patientId, contours, studyDate, studyTime):
        data = self.dbConnection["data"]
        segImage = Struct()
    
        #TODO Ideally, we should change RTReferencedSeriesSequence.SeriesInstanceUID, etc. with the correct id in the DB... In the meantime provide all the necessary metaData...

        # segImage.loadStruct(structFileName, ctMeta)
        segImage.loadStructByPatientContours(structFileName, ctMeta, patientId, contours, studyDate, studyTime)

        segCol = segImage.getMeta()

        segId = self.insertData(segCol)["id"]
    
        return {"id": str(segId), "meta": segCol}

    def insertWorkflowEntry(self, seriesId, segId, users, meta, info, parents, label):
        if(not seriesId):
            seriesId = []
        if(not segId):
            segId = ""
        if(not users):
            users = []
        if(not parents):
            parents = []
        
        #TODO: convert parents, seriesId, and segId to Strings if they are ObjectIds

        workflows = self.dbConnection["workflows"]

        ts = time.time()
        isodate = datetime.datetime.fromtimestamp(ts, None)

        if isinstance(meta, dict) and not isinstance(meta["PatientID"], pydicom.DataElement):
            col = {
                "parents": parents,
                "date": isodate,
                "users": users,
                "seriesId": seriesId,
                "segId": segId,
                "PatientName": str(meta["PatientName"]),
                "PatientID": meta["PatientID"],
                "PatientSex": str(meta["PatientSex"]),
                "PatientAge": str(meta["PatientAge"]),
                "PatientWeight": str(meta["PatientWeight"]),
                "BodyPartExamined": str(meta["BodyPartExamined"]),
                "info": str(info),
                "label": label
                }
        else:
            col = {
                "parents": parents,
                "date": isodate,
                "users": users,
                "seriesId": seriesId,
                "segId": segId,
                "PatientName": str(meta.PatientName),
                "PatientID": meta.PatientID,
                "PatientSex": str(meta.PatientSex),
                "PatientAge": str(meta.PatientAge),
                "PatientWeight": str(meta.BodyPartExamined) if hasattr(meta, 'PatientWeight') else "",
                "BodyPartExamined": str(meta.BodyPartExamined) if hasattr(meta, 'BodyPartExamined') else "",
                "info": str(info),
                "label": label
            }    

        entryId = workflows.insert(col)

        if (not len(parents)):
            workflowId = str(entryId)
        else:
            parentEntry = getWorkflowEntry(parents[0])
            workflowId = parentEntry["workflowId"]

        workflows.update_one({"_id": entryId}, {"$set": {"workflowId": workflowId}})

        return str(entryId)
    
    def insertData(self, seriesMeta, seriesData=None):
        data = self.dbConnection["data"]
        ctCols = None
        
        if (seriesData is None):
            ctCols = convertMetaToCollection(seriesMeta)
            
            if "_id" in ctCols:
                del ctCols["_id"]
            
            dataId = data.insert(ctCols)
        else:
            seriesId = None
            
            # TODO Use reshape instead of a loop
            ct = []
            for i in range(seriesData.shape[-1]):
                newData = seriesData[:, :, i].flatten()
                if (i==0):
                    ct = newData
                else:
                    ct = np.concatenate((ct, newData))
            
            ct = ct.astype(np.int16).tobytes()
            
            ctCols = []
            for i, meta in enumerate(seriesMeta):
                print("Import slice " + str(i+1) + "/" + str(len(seriesMeta)), file=sys.stderr)

                ctCol = convertMetaToCollection(meta)
                ctCol["dataPath"] = self.dataPath
            
                if "_id" in ctCol:
                    del ctCol["_id"]
            
                ctCols.append(ctCol)
                instanceId = data.insert(ctCol)
            
                stride = ctCol["Rows"]*ctCol["Columns"]*2;
            
                if (i==0):
                    seriesId = instanceId
                    myquery = { "_id": seriesId }
                    
                newvalues = { "$set": { "seriesId": str(seriesId), "instanceId": str(instanceId), "sliceNb": i } }
                data.update_one({"_id": instanceId}, newvalues)
            
                writeImage(self.dataPath, ctCol, ct[i*stride:(i+1)*stride], str(seriesId), str(instanceId), str(ctCol["SliceLocation"]), sftp=self.sftp)    
            
                dataId = seriesId
            
        return {"id": str(dataId), "meta": ctCols}

