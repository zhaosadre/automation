import os, sys
import pydicom
import json

def getPatients():
    patients = []
    patientsName = []

    for root, dirs, files in os.walk(uploadedDataPath):
        for fileName in files:
            if fileName.endswith('.dcm') or fileName.endswith('.DCM'):
                filePath = os.path.join(root, fileName)
                dcm = pydicom.read_file(filePath)
                if dcm.Modality == 'RTSTRUCT':
                    name = dcm.PatientName.given_name + dcm.PatientName.family_name
                    folderName = os.path.basename(root)
                    if (name not in patientsName):
                        patient = {
                            "name": name,
                            "studydate": dcm.StudyDate,
                            "studytime": dcm.StudyTime, 
                            "id" : dcm.PatientID,
                            "foldername": folderName
                        }
                        patients.append(patient)
                        patientsName.append(name)
    return patients

# run the function
uploadedDataPath = sys.argv[1]
patients = getPatients()
print(json.dumps(patients))

