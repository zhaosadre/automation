import os, sys
import pymongo
from workflow import Workflow
import pydicom
import numpy as np
import json
from help import writeToFile, checkFileValid

def createWorkflow(ctSeriesFilePath, rsSegFilePath, contourROINumbers, patientId, studyDate, studyTime, wfDescription, wfLabel):

    rsResult = checkFileValid(rsSegFilePath, "RTSTRUCT")
    ctResult = checkFileValid(ctSeriesFilePath, "CT")

    stdout = ""
    if(ctResult=="FileNoFound" or rsResult=="FileNoFound" or rsResult=="wrongRTSTRUCT" or ctResult=="wrongCT"):
        return {"status": "400", "message": "Selected files are wrong!"}
    else:
        wf = Workflow()
        # insert dcm series
        seriesInfo = wf.insertDcmSeries([ctSeriesFilePath,])

        # insert dcm struct
        segInfo = wf.insertDcmStruct(rsSegFilePath, seriesInfo["meta"][0], patientId, contourROINumbers, studyDate, studyTime)

        seriesIds = seriesInfo["id"]
        seriesMeta = seriesInfo["meta"]
        seriesData = seriesInfo["data"]

        # Study data
        studyMeta = seriesMeta[0][0]
        
        # Seg information
        segId = segInfo["id"]
        segMeta = segInfo["meta"]
        
        info = ""
        if wfDescription and len(wfDescription)>0:
            info = wfDescription
            
        label = ""
        if wfLabel and len(wfLabel)>0:
            label = wfLabel

        rootId = wf.insertWorkflowEntry(seriesIds, segId, None, studyMeta, info, None, label)
        return {"status": "200", "message": "Successful"}
    

# run the function
ctSeriesFilePath = sys.argv[1]
rsSegFilePath = sys.argv[2]
contourROINumbers= sys.argv[3].split(",")
patientId = sys.argv[4]
studyDate = sys.argv[5]
studyTime = sys.argv[6]
wfDescription = sys.argv[7]
wfLabel = sys.argv[8]

data = createWorkflow(ctSeriesFilePath, rsSegFilePath, contourROINumbers, patientId, studyDate, studyTime, wfDescription, wfLabel)
print(json.dumps(data))

