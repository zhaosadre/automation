import json
import os
import pydicom
import yaml

def readConfigData():
    env = os.environ.get("NODE_ENV")
    if(env == "development"):
        writeToFile("dev.txt", env)
        with open(r'config/config_dev.yaml') as file:
            config = yaml.load(file, Loader=yaml.FullLoader)
            return config
    else:
        with open(r'config/config_prod.yaml') as file:
            config = yaml.load(file, Loader=yaml.FullLoader)
            return config

def writeToFile(filename, data):
    testLogFile = "C:\\Users\\wei\\Documents\\Work\\UCLouvain\\CHARP_Captain\\automation\\server"
    if not os.path.exists(os.path.join(testLogFile, filename)):
        with open(filename, 'w') as outfile:
            print(data,file=outfile)
    else:
        os.remove(os.path.join(testLogFile, filename))
        writeToFile(filename, data)


def checkFileValid(filePath, modalidy):
    if (os.path.isfile(filePath)):
        dcm = pydicom.read_file(filePath)
        if dcm and dcm.Modality == modalidy:
            return "OK"
        else:
            return "wrong"+modalidy
    else:
        return "FileNotFound"