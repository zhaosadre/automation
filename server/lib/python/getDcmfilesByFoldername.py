import os, sys
import pydicom
import json
from help import writeToFile

def getDcmFilesByFoldername(folderPath):
    dcmFiles = []
    for root, dirs, files in os.walk(folderPath):
        for fileName in files:
            if fileName.endswith('.dcm') or fileName.endswith('.DCM'):
                filePath = os.path.join(root,fileName)
                dcm = pydicom.read_file(filePath)
                dcmFile = {
                    "fileName": fileName,
                    "filePath": filePath,
                    "modality": dcm.Modality
                }
                dcmFiles.append(dcmFile)

    if not dcmFiles:
        return {"status": "400", "message": "No dcm file in the server", "files": dcmFiles}
    else:
        return {"status": "200", "message": "Sucessful", "files": dcmFiles}

# run
folderPath = sys.argv[1]
dcmFiles = getDcmFilesByFoldername(folderPath)
print(json.dumps(dcmFiles))
