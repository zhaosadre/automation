import os
import pymongo
from ssh_pymongo import MongoSession
import pysftp
from bson.objectid import ObjectId
from help import readConfigData, writeToFile

class DB:
  config = readConfigData()
  dataPath = config['dataPath']
  port = int(config['port'])
  dbName=config['dbName']
  host=config['host']
  to_host=config['to_host']
  to_port=config['to_port']
  mongoDBClient=str(config['mongoDBClient'])

  def __init__(self, dataPath=dataPath, dbName=dbName, host=host, user=None, password=None, key=None, port=22, to_host=to_host, to_port=to_port, mongoDBClient=mongoDBClient):
    if (not (host=="127.0.0.1" or host=="localhost")):
      self.session = MongoSession(host, user, password, key=key, uri=None, port=port, to_host=to_host, to_port=to_port)
      self.db = self.session.connection[dbName]
      self.sftp = pysftp.Connection(host, username=user, password=password, private_key=key)
    else:
      self.session = None
      self.sftp = None
      self.db = pymongo.MongoClient(mongoDBClient)[dbName]
    
    self.dataPath = dataPath;

  def connection (self):
    return self.db

  def getSftp(self):
    return self.sftp
    
  def __del__(self):
    if (self.sftp):
      self.session.stop()
      self.sftp.close()
  
