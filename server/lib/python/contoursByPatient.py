import os, sys 
import pydicom
import json

def getContoursByPatient():                                
    contours = []
    for root, dirs, files in os.walk(uploadedDataPath):
        for fileName in files:
            if fileName.endswith('.dcm') or fileName.endswith('.DCM'):
                filePath = os.path.join(root,fileName)
                dcm = pydicom.read_file(filePath)
                if dcm.Modality == 'RTSTRUCT':
                    if (patientId == dcm.PatientID and 
                        studyDate == dcm.StudyDate and 
                        studyTime == dcm.StudyTime ):
                        for roi in dcm.StructureSetROISequence:
                            contour = {
                                "roiname" : roi.ROIName,
                                "roinumber" : str(roi.ROINumber)
                            }
                            contours.append(contour)
    return contours

# run
uploadedDataPath = sys.argv[1]
patientId = sys.argv[2]
studyDate = sys.argv[3]
studyTime = sys.argv[4]
contours = getContoursByPatient()
print(json.dumps(contours))


