import os, sys
import json
from help import writeToFile

def getServerFolders(dataPath):
    contents=[]
    contents=os.listdir(dataPath)
    if not contents:
        return {"status": "400", "message": "No uploaded folders in the server", "folders": []}
    else:
        return {"status": "200", "message": "Sucessful", "folders": contents}
# run
dataPath = sys.argv[1]
contents = getServerFolders(dataPath)
print(json.dumps(contents))

    
