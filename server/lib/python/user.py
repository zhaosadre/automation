import hashlib
import random
from db import DB
from help import readConfigData

class User:
    config = readConfigData() 
    db = DB(dataPath=config['imageDataPath'], dbName=config['dbName'])
    def getUsers(self):

        mycol = self.db["users"]
        users = mycol.find().sort("username")
        return list(users)

    def getUserNames(self):
        users = getUsers()
        userNames = []
        for user in users:
            userNames.append(str(user['username']))

        return userNames

    def insertUser(self, username, pwd, role):
        salt = str(random.getrandbits(128))
        
        hash = hashlib.pbkdf2_hmac('sha512', pwd.encode(), salt.encode(), 1000).hex()

        users = self.db["users"]
        users.insert({
            "username": username,
            "salt": salt,
            "hash": hash,
            "role": role
        })