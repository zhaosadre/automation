# REST API

## Login

POST /api/login
	Body application/json: { username, password }
Result:
   200: successful (with Token)
   
## Logout

POST /api/logout
Result:
	302: successful, redirect to "/"
	
## Clean unsafe

PUT /api/admin/cleanunsafe
Result:
	200: successful

## Folder
GET /api/folders
	200:  JSON list of folders
	401, 405, 500:  { message: "errormessage" }

POST /api/folder/[folder]
	200： 
	400
GET /api/folder/[folder]
	200：
	400
DELETE /api/folder/[folder]
	200： 
	400
DELETE /api/folder/[folder]/[file]
	200：
	400

## Add public key

POST /api/admin/addpublickey?publicKey=[key]
	300：
	--
Result:
	200: successful
	??
## Create user

POST /api/admin/signup
	200: Body application/json: { username, password, role }
	??                                     
Result:
	200: successfull, returns { done: true }
	500: error
	
## Workflow

GET /api/workflow/[id]
Result:
	200: successful, returns workflow data
	
DELETE /api/workflow/[id]

Result:
	200: successful, workflow deleted
	
POST /api/workflow/[id]/users/[user]
	200: successful, the user of the workflow is added

POST /api/patient/[id]/new
	200: successful, created a new workflow for the patient
	
## Workflow Entry

GET /api/workflowentry/[id]
	200: successful, get the workflow entry
	
DELETE /api/workflowentry/[id]
   200: successful, delete the selected workflow entry
	
POST /api/workflowentry/[id]/reviewer?user=[user]
	200: successful, add a new reviewer to the workflow
	
Result:
	200: successful, create a new workflow entry with the reviewer 

## Segment

GET /api/segment/[segid]/metadata
Result:
	200: application/json metadata
	401: 
	405: 
	
PUT /api/segment/[segid]/metadata
	Body: application/json { metaData: [metadata] , data:null }
	200: 

## Series

GET /api/series/[seriesid]/instance/[instanceid]
	200: 
	401:
	501:

GET /api/series/[seriesid]/metadata
	200: 
	401:
	400:

## Patient
GET /api/patients
GEt /api/patient/[id]/contours







